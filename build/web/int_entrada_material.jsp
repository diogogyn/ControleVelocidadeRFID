<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@page contentType="text/html"%>

<html lang="en">
    <!--/modulos/entrada/entrada_inclusao.php-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Bootstrap Admin Theme</title>

        <!-- Bootstrap Core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">SB Admin v2.0</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>Read All Messages</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks">
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 1</strong>
                                            <span class="pull-right text-muted">40% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 2</strong>
                                            <span class="pull-right text-muted">20% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 3</strong>
                                            <span class="pull-right text-muted">60% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                <span class="sr-only">60% Complete (warning)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 4</strong>
                                            <span class="pull-right text-muted">80% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Tasks</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <!-- MENU -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="${menu.INICIO}"><i class="fa fa-dashboard fa-fw"></i> Pagina Inicial</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw"></i> CADASTRO<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${menu.CAD_PROFISSIONAL}">Profissional</a>
                                    </li>
                                    <li>
                                        <a href="${menu.CAD_PACIENTE}">Pacientes</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw"></i> RECEITA<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${menu.RECEITA}${menu.PARAM}${menu.REC_NOVA}">Nova</a>
                                    </li>
                                    <li>
                                        <a href="${menu.RECEITA}${menu.PARAM}${menu.REC_COMPLEMENTAR}">Complementar</a>
                                    </li>
                                    <li>
                                        <a href="${menu.RECEITA}${menu.PARAM}${menu.REC_FINALIZAR}">Finalizar</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw"></i> INTEGRA��O SIST. RFID<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${menu.INTEGRACAO}${menu.PARAM}${menu.INT_ENTRADA_MATERIAL}">Entrada de Material RFID</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw"></i> CONSULTA/RELAT�RIO<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${menu.RELATORIO}${menu.PARAM}${menu.REL_ESTOQUE}">Estoque</a>
                                    </li>
                                    <li>
                                        <a href="${menu.RELATORIO}${menu.PARAM}${menu.REL_LIV_MEDICAMENTOS}">Livro de Medicamentos Controlados</a>
                                    </li>
                                    <li>
                                        <a href="${menu.RELATORIO}${menu.PARAM}${menu.REL_RECIBO_RECEITA}">Recibo da Receita</a>
                                    </li>
                                    <li>
                                        <a href="${menu.RELATORIO}${menu.PARAM}${menu.REL_MEDICAMENTOS}">Medicamentos</a>
                                    </li>
                                    <li>
                                        <a href="${menu.RELATORIO}${menu.PARAM}${menu.REL_PACIENTES}">Pacientes</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw"></i> ADMINISTRA��O<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${menu.ADMINISTRACAO}${menu.PARAM}${menu.ADM_ADD_USUARIO}">Cadastrar Usu�rios</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Movimenta�ao</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Entrada de material
                            </div>
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <li><a href="#entrada" data-toggle="tab">Recebimento de Medicamentos</a>
                                    </li>
                                    <li><a href="#saida" data-toggle="tab">Saida</a>
                                    </li>
                                    <li><a href="#itens" data-toggle="tab">Itens de Estoque</a>
                                    </li>
                                    <li><a href="#grupo" data-toggle="tab">Grupo de itens</a>
                                    </li>
                                    <li><a href="#cadastros" data-toggle="tab">Cadastros</a>
                                    </li>
                                    <li><a href="#usuarios" data-toggle="tab">Usu�rios</a>
                                    </li>
                                    <li><a href="#relatorios" data-toggle="tab">Relat�rios</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    
                                    <div class="tab-pane fade in active" id="entrada">
                                        <h4>Procedimento de entrada de medicamentos para estoque</h4>
                                        <p>Controle de entrada de medicamento para estocagem na farmacia</p>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <form role="form">
                                                                <div class="col-lg-4">
                                                                    <div class="form-group">
                                                                        <label>Numero do documento</label>
                                                                        <input type="text" name="num_documento" placeholder="00000000" required="" class="form-control">
                                                                        <p class="help-block">Informe o numero do Doc. de entrada.</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div class="form-group">
                                                                        <br>
                                                                        <button type="submit" class="btn btn-primary">OK</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <br><p class="btn btn-success">FINALIZADA!</p>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- /.row (nested) -->
                                                        
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="panel-body">
                                                                    <div class="dataTable_wrapper">
                                                                        <table class="table table-striped table-bordered table-hover" id="dataTables-listaItens">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>N� DANFE</th>
                                                                                    <th>C�D. MED</th>
                                                                                    <th>Desc. Produo</th>
                                                                                    <th>UND ENT</th>
                                                                                    <th>UND SAI</th>
                                                                                    <th>LOTE</th>
                                                                                    <th>VALIDADE</th>
                                                                                    <th>LOCAL ARMAZ.</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <c:forEach var="listaItens" items="${listaItens}">
                                                                                    <tr class="odd gradeX">
                                                                                        <td>${listaItens.numDanfe}</td>
                                                                                        <td>${listaItens.codigoMedicamento}</td>
                                                                                        <td>${listaItens.nomeMedicamento}</td>
                                                                                        <td>${listaItens.unidEntrada}</td>
                                                                                        <td>${listaItens.unidSaida}</td>
                                                                                        <td>${listaItens.lote}</td>
                                                                                        <td>${listaItens.dtValidade}</td>
                                                                                        <td class="center">
                                                                                            <p class="btn btn-outline btn-info btn-xs">${listaItens.area}</p>
                                                                                            <p class="btn btn-outline btn-primary btn-xs">${listaItens.rua}</p>
                                                                                            <p class="btn btn-outline btn-danger btn-xs">${listaItens.modulo}</p>
                                                                                            <p class="btn btn-outline btn-default btn-xs">${listaItens.nivel}</p>
                                                                                            <p class="btn btn-outline btn-warning btn-xs">${listaItens.vao}</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </c:forEach>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- /.table-responsive -->
                                                                    <div class="well">
                                                                        <div class="row">
                                                                            <div class="col-lg-2">
                                                                                <form role="form" method="post" action="Relatorio.rfid?pagina=${menu.REL_ENT_MEDICAMENTOS}&num_danfe=${listaItens.numDanfe}">
                                                                                    <button type="submit" class="btn btn-primary" <c:if test="${btnGerarRelEntrad!=false}">disable</c:if>>Gerar Relatorio</button>
                                                                                </form>
                                                                            </div>
                                                                            <div class="col-lg-2">
                                                                                <form role="form" method="post" action="Relatorio.rfid?pagina=${menu.REL_ENT_MEDICAMENTOS}&num_danfe=${listaItens.numDanfe}">
                                                                                    <button type="submit" class="btn btn-success" <c:if test="${btnGerarRelEntrad!=false}">disable</c:if>>Finalizar Documento</button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- /.panel-body -->
                                                            </div>
                                                            <!-- /.panel -->
                                                            <!-- /.col-lg-12 -->
                                                        </div>
                                                        <!-- /.row -->
                                                    </div>
                                                    <!-- /.panel-body -->
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                            <!-- /.col-lg-12 -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <div class="tab-pane fade" id="saida">
                                        <h4>Historico de Retiradas de estoque automatizado</h4>
                                        <p>Controle de estoque sobre as entradas e saidas de estoque</p>
                                        <p>Esta rotina ser� atualizada de acordo com a atualiza��o do RFID. 
                                            A cada leitura, o sistema computar� a quantidade de medicamentos que foram retiados
                                            verificando se os mesmos foram retirados devidamento ou estraviados.
                                        </p>
                                        <p>
                                            Ser� expedido um relatorio com as justificativas e destinos de cada medicamento retirado do sistema.
                                            Esta rotina ser� executada por uma leitora instalada dentro da sala de estocagem
                                        </p>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="panel-body">
                                                                    <div class="dataTable_wrapper">
                                                                        <table class="table table-striped table-bordered table-hover" id="dataTables-Saidas">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>DATA</th>
                                                                                    <th>PROTOCOLO</th>
                                                                                    <th>RETIRADAS</th>
                                                                                    <th>SITUA��O</th>
                                                                                    <th></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <c:forEach var="listaSaidas" items="${listaSaidas}">
                                                                                    <tr class="gradeA">
                                                                                        <td>${listaSaidas.protocolo}</td>
                                                                                        <td>${listaSaidas.data}</td>
                                                                                        <td>${listaSaidas.qtde} UNIDS.</td>
                                                                                        <td><p class="btn btn-outline btn-success btn-xs ">${listaSaidas.situacao}</p></td>
                                                                                        <td><p class="btn btn-outline btn-default btn-xs ">RELAT�RIO</p></td>
                                                                                    </tr>
                                                                                </c:forEach>
                                                                                <tr class="gradeA">
                                                                                    <td>21/06/2016</td>
                                                                                    <td>32165498</td>
                                                                                    <td>30 UNIDS.</td>
                                                                                    <td><button type="button" class="btn btn-outline btn-danger btn-xs ">Contigencia</button></td>
                                                                                    <td><p class="btn btn-outline btn-default btn-xs ">RELAT�RIO</p></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- /.table-responsive -->
                                                                </div>
                                                                <!-- /.panel-body -->
                                                            </div>
                                                            <!-- /.col-lg-12 -->
                                                        </div>
                                                        <!-- /.row -->
                                                    </div>
                                                    <!-- /.panel-body -->
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                            <!-- /.col-lg-12 -->
                                        </div>
                                        <!-- /.row --> 
                                    </div>
                                    <div class="tab-pane fade" id="itens">
                                        <h4>Settings Tab</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                    <div class="tab-pane fade" id="grupo">
                                        <h4>Settings Tab</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                    <div class="tab-pane fade" id="cadastros">
                                        <h4>Settings Tab</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                    <div class="tab-pane fade" id="usuario">
                                        <h4>Settings Tab</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                    <div class="tab-pane fade" id="relatorios">
                                        <h4>Settings Tab</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function () {
                $('#dataTables-example').DataTable({
                    responsive: true
                });
            });
            $(document).ready(function () {
                $('#dataTables-listaItens').DataTable({
                    responsive: true
                });
            });
            $(document).ready(function () {
                $('#dataTables-Saidas').DataTable({
                    responsive: true
                });
            });
        </script>

    </body>

</html>



