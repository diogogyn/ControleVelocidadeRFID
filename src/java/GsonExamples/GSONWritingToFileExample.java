/*
 * http://www.java2blog.com/2013/11/gson-example-read-and-write-json.html
 */
package GsonExamples;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import java.util.Random;

/* 
 * @Author : Arpit Mandliya 
 */
public class GSONWritingToFileExample {

    public static void main(String[] args) {
        Random m = new Random();

        // convert java object to JSON format,  
        // and returned as JSON formatted string  
        try {
            List<String> listOfStates = new ArrayList<String>();
            listOfStates.add("Madhya Pradesh");
            listOfStates.add("Maharastra");
            listOfStates.add("Rajasthan");

            FileWriter writer = new FileWriter("//Users//solange//CountryGSON.json");
            for (int i = 0; i <= 5; i++) {
                Country countryObj = new Country();
                countryObj.setName("Pais " + m.nextInt(10));
                countryObj.setPopulation(m.nextInt(100000));
                countryObj.setListOfStates(listOfStates);

                Gson gson = new Gson();
                String json = gson.toJson(countryObj);

            //write converted json data to a file named "CountryGSON.json"  
                writer.write(json);
                System.out.println(json);
            }

            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        

    }
}
