package ObjetosViaSocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import middleware.EPCs;

public class Cliente {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        EPCs teste = new EPCs();
        teste.setEpc("Cabecalho");
        Socket client = new Socket("127.0.0.1", 7001);
        ObjectOutputStream output = new ObjectOutputStream(client.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(client.getInputStream());
        output.flush();
        output.writeObject(teste);
        output.flush();
        output.close();
        EPCs testeModificado = (EPCs) input.readObject();
        System.out.println(testeModificado);
    }
}
