/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjetosViaSocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import middleware.EPCs;

public class Servidor {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ServerSocket server = new ServerSocket(7001);
        Socket connection = server.accept();
        ObjectInputStream input = new ObjectInputStream(connection.getInputStream());
        ObjectOutputStream output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
        EPCs teste = (EPCs) input.readObject();
        System.out.println(teste.getEpc());
        teste.setEpc("Cabecalho");
        output.writeObject(new EPCs());
        output.flush();
        output.close();
    }
}
