package UsuarioLocal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import UsuarioLocal.FiltroLocal.Usuario;
import aplicacao.dao.Criptografia;
import conexao.FabricaConexao;

/**
 *
 * @author Diogo
 */
public class UsuarioLocalDAO {

    private final Connection con;

    public UsuarioLocalDAO(Connection conexao) throws SQLException {
        this.con = conexao;
    }

    public UsuarioLocalDAO() throws SQLException {
        this.con = FabricaConexao.getConnection();
    }

    public Usuario getUsuarioLocal(String chave) throws SQLException {
        Usuario local = new Usuario();
        String sql = "SELECT id_profissional, nome_profissional, nivel_acesso, login FROM profissionais  WHERE login= ? ";
        PreparedStatement psmt = con.prepareStatement(sql);
        psmt.setString(1, chave);
        ResultSet rs = psmt.executeQuery();
        while (rs.next()) {
            local.setId_usuario(rs.getInt("id_profissional"));
                local.setLogin(rs.getString("login"));
                local.setNome(rs.getString("nome_profissional"));
                local.setNivelCodigo(rs.getString("nivel_acesso"));
            // local.setNivelCodigo(rs.getString("nivel_codigo"));
        }
        return local;
    }

    public Usuario getUsuarioLocal(String login, String senha) throws SQLException {
        Usuario local = new Usuario();
        String sql = "SELECT id_profissional, nome_profissional, nivel_acesso FROM profissionais WHERE login = ? and senha = ?";
        PreparedStatement psmt = con.prepareStatement(sql);
        psmt.setString(1, login.toUpperCase());
        psmt.setString(2, Criptografia.criptografar(senha.toLowerCase()));
        try {
            try (ResultSet rs = psmt.executeQuery()) {
                while (rs.next()) {
                    local.setId_usuario(rs.getInt("id_profissional"));
                    local.setLogin(login.toLowerCase());
                    local.setNome(rs.getString("nome_profissional"));
                    local.setNivelCodigo(rs.getString("nivel_acesso"));
                    local.setSituacao("A");
                    local.setFlagErro(false);
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            local.setFlagErro(true);
        } finally {
            psmt.close();
        }
        return local;
    }

    public Usuario getUsuarioLocal(int id) throws SQLException {
        Usuario local = new Usuario();
        String sql = "SELECT id_profissional, nome_profissional, nivel_acesso, login FROM profissionais WHERE id_usuario = ? ";
        PreparedStatement psmt = con.prepareStatement(sql);
        psmt.setInt(1, id);
        try {
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                local.setId_usuario(rs.getInt("id_profissional"));
                local.setLogin(rs.getString("login"));
                local.setNome(rs.getString("nome_profissional"));
                local.setNivelCodigo(rs.getString("nivel_acesso"));
                // local.setNivelCodigo(rs.getString("nivel_codigo"));
            }
        } catch (SQLException e) {
            System.out.println("ERRO: " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("ERRO: " + e.getMessage());
        }
        return local;
    }
}
