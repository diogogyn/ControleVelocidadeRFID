/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package aplicacao.dao;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author solange
 */
class Carro {

    private Date momentoPassagem;
    private Double hashCode = Math.random();
    
    Carro(Date date) {
        this.momentoPassagem = date;
    }

    Carro(Date date, Double hashcode) {
        this.momentoPassagem = date;
        this.hashCode = hashcode;
    }

    Carro(Double hashcode) {
        this.hashCode = hashcode;
    }
    
    public Date getMomentoPassagem(){
        return momentoPassagem;
    }
    
    public Double getHashCode(){
        return hashCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.hashCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carro other = (Carro) obj;
        if (!Objects.equals(this.hashCode, other.hashCode)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
