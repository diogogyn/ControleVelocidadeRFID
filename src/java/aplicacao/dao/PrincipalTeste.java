package aplicacao.dao;

import java.util.ArrayList;
import middleware.Carros;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import middleware.PreencheLista;
import middleware.EPCs;
import middleware.FactoryObjeto;

/**
 *
 * @author rfid
 */
public class PrincipalTeste {

    PreencheLista mt2 = new PreencheLista(this);
    String dadosParaEnvio = "";

    public void add() {
        listaItensEPCAntena1.add(FactoryObjeto.getValue("0446"));
        listaItensEPCAntena1.add(FactoryObjeto.getValue("0446"));
    }

    public void processaDadosParaEnvio() {
        for (EPCs list : listaItensParaEnvio) {
            dadosParaEnvio = dadosParaEnvio + list.getEpc() + "|" + list.getHorario1() + "|" + list.getHorario2() + ";\n";
        }
    }

    public boolean enviaDadosServidorProcessamento() {
        //criar algorimo para conexao com servidor e envio dos dados
        this.processaDadosParaEnvio();
        for (EPCs list : listaItensParaEnvio) {
            System.out.println("Veiculo: " + list.getEpc() + " entre " + list.getHorario1() + " e " + list.getHorario2());
        }
        //System.out.println("----------------------------------------------------");
        //System.out.println("Token a ser enviado: ");
        //System.out.println(dadosParaEnvio);
        listaItensParaEnvio.clear();
        return true;
    }

    public static void main(String[] args) {
        Set<Carros> listaItensEPCAntena1 = new HashSet<>();
        Set<Carros> listaItensEPCAntena2 = new HashSet<>();
        Set<EPCs> listaItensParaEnvio = new HashSet<>();
    
        for (Carros listAntena1 : listaItensEPCAntena1) {
            //System.out.println("fazendo a interacao");
            List list1 = new ArrayList<>(listaItensEPCAntena1);
            Carros interesse = new Carros(listAntena1.getHashCode());

            if (listaItensEPCAntena1.contains(interesse) && listaItensEPCAntena2.contains(interesse)) {
                //System.out.println("Item esta nas listas");
                Carros a = (Carros) list1.remove(list1.indexOf(new Carros(listAntena1.getHashCode())));

                List list2 = new ArrayList<>(listaItensEPCAntena2);
                Carros b = (Carros) list2.remove(list2.indexOf(a));
                listaItensParaEnvio.add(
                        new EPCs(a.getHashCode(),/*EPC*/
                                a.getMomentoPassagem(),/*HORARIO 1ª LEITURA*/
                                b.getMomentoPassagem()));/*HORARIO 2ª LEITURA*/

                //System.out.println("Veiculo: " + a.getHashCode() + " entre " + a.getMomentoPassagem() + " e " + b.getMomentoPassagem());
            }
        }
        //dao.realizarEnvioDados(this);
        if (listaItensParaEnvio.size() > 0) {
            System.out.println("QTDE a enviar: " + listaItensParaEnvio.size());
            enviaDadosServidorProcessamento();
        } else {
            System.out.println("Nenhum registro para ser enviado!");
        }

    }
}
