package aplicacao.modelo;

/**
 *
 * @author Diogo
 */
public class Menu {

    private final String PAGINA;
    private final String PARAM;
    private final String INICIO;
    private final String LOGIN;

    private final String CAD_PROFISSIONAL;
    private final String CAD_ADD_PROFISSIONAL;

    private final String CAD_PACIENTE;
    private final String CAD_ADD_PACIENTE;

    private final String RECEITA;
    private final String REC_NOVA;
    private final String REC_COMPLEMENTAR;
    private final String REC_FINALIZAR;
    private final String REC_BUSCAR_PACIENTE;
    private final String REC_BUSCAR_RECEITAS;
    private final String REC_BUSCAR_ITENS_RECEITA;
    private final String REC_ADD_ITEM;
    private final String REC_DEL_ITEM;

    private final String INTEGRACAO;
    private final String INT_ENTRADA_MATERIAL;

    private final String MOVIMENTACAO;
    private final String MOV_ENTRADA_MAT;
    private final String MOV_AJUSTAR_ESTOQUE;
    private final String RELATORIO;
    private final String REL_ESTOQUE;
    
    private final String REL_ENT_MEDICAMENTOS;
    private final String REL_RECIBO_RECEITA;
    private final String REL_MEDICAMENTOS;
    private final String REL_PACIENTES;

    private final String ADMINISTRACAO;
    private final String ADM_ADD_USUARIO;
    private final String CAD_EDT_PACIENTE;
    private final String CAD_EDT_PROFISSIONAL;

    public Menu() {
        this.INICIO = "Inicio.rfid";
        this.LOGIN = "Autenticacao.rfid";

        this.CAD_PROFISSIONAL = "Profissional.rfid";

        this.CAD_PACIENTE = "Paciente.rfid";

        this.RECEITA = "Receita.rfid";

        this.INTEGRACAO = "Integracao.rfid";

        this.MOVIMENTACAO = "404.rfid";
        this.MOV_ENTRADA_MAT = "erro";
        this.MOV_AJUSTAR_ESTOQUE = "";
        this.RELATORIO = "Relatorio.rfid";

        this.REL_RECIBO_RECEITA = "relReciboReceita";
        this.REL_MEDICAMENTOS = "relMedic";

        this.ADMINISTRACAO = "404.rfid";

        //----------------------------------------------------
        this.PAGINA = "pagina";
        this.PARAM = "?" + this.PAGINA + "=";

        this.CAD_ADD_PROFISSIONAL = "addProfissional";
        this.CAD_EDT_PROFISSIONAL = "edtProfissional";
        this.CAD_ADD_PACIENTE = "addPaciente";
        this.CAD_EDT_PACIENTE = "edtPaciente";
        this.ADM_ADD_USUARIO = "erro";

        this.REL_ESTOQUE = "relEstoque";
        this.REL_ENT_MEDICAMENTOS = "relEntMedic";
        this.REL_PACIENTES = "relPacientes";
        this.REC_NOVA = "addReceita";

        this.REC_COMPLEMENTAR = "edtReceita";
        this.REC_FINALIZAR = "finalizacaoReceita";
        this.REC_BUSCAR_PACIENTE = "buscarPaciente";
        this.REC_BUSCAR_RECEITAS = "buscarReceitas";
        this.REC_BUSCAR_ITENS_RECEITA = "buscaItensReceita";
        this.REC_ADD_ITEM = "addItem";
        this.REC_DEL_ITEM = "delItem";

        this.INT_ENTRADA_MATERIAL = "buscaDocumento";
    }

    public String getCAD_EDT_PACIENTE() {
        return CAD_EDT_PACIENTE;
    }

    public String getCAD_EDT_PROFISSIONAL() {
        return CAD_EDT_PROFISSIONAL;
    }

    public String getPAGINA() {
        return PAGINA;
    }

    public String getPARAM() {
        return PARAM;
    }

    public String getINICIO() {
        return INICIO;
    }

    public String getLOGIN() {
        return LOGIN;
    }

    public String getCAD_PROFISSIONAL() {
        return CAD_PROFISSIONAL;
    }

    public String getCAD_ADD_PROFISSIONAL() {
        return CAD_ADD_PROFISSIONAL;
    }

    public String getCAD_PACIENTE() {
        return CAD_PACIENTE;
    }

    public String getCAD_ADD_PACIENTE() {
        return CAD_ADD_PACIENTE;
    }

    public String getRECEITA() {
        return RECEITA;
    }

    public String getREC_NOVA() {
        return REC_NOVA;
    }

    public String getREC_COMPLEMENTAR() {
        return REC_COMPLEMENTAR;
    }

    public String getREC_FINALIZAR() {
        return REC_FINALIZAR;
    }

    public String getINTEGRACAO() {
        return INTEGRACAO;
    }

    public String getINT_ENTRADA_MATERIAL() {
        return INT_ENTRADA_MATERIAL;
    }

    public String getMOVIMENTACAO() {
        return MOVIMENTACAO;
    }

    public String getMOV_ENTRADA_MAT() {
        return MOV_ENTRADA_MAT;
    }

    public String getMOV_AJUSTAR_ESTOQUE() {
        return MOV_AJUSTAR_ESTOQUE;
    }

    public String getRELATORIO() {
        return RELATORIO;
    }

    public String getREL_ESTOQUE() {
        return REL_ESTOQUE;
    }

    public String getREL_ENT_MEDICAMENTOS() {
        return REL_ENT_MEDICAMENTOS;
    }

    public String getREL_RECIBO_RECEITA() {
        return REL_RECIBO_RECEITA;
    }

    public String getREL_MEDICAMENTOS() {
        return REL_MEDICAMENTOS;
    }

    public String getREL_PACIENTES() {
        return REL_PACIENTES;
    }

    public String getADMINISTRACAO() {
        return ADMINISTRACAO;
    }

    public String getADM_ADD_USUARIO() {
        return ADM_ADD_USUARIO;
    }

    public String getREC_BUSCAR_PACIENTE() {
        return REC_BUSCAR_PACIENTE;
    }

    public String getREC_BUSCAR_RECEITAS() {
        return REC_BUSCAR_RECEITAS;
    }

    public String getREC_BUSCAR_ITENS_RECEITA() {
        return REC_BUSCAR_ITENS_RECEITA;
    }

    public String getREC_ADD_ITEM() {
        return REC_ADD_ITEM;
    }

    public String getREC_DEL_ITEM() {
        return REC_DEL_ITEM;
    }
    

}
