package integracao.dao;

import java.util.List;
import aplicacao.dao.Funcoes;
import conexao.FabricaConexao;
import integracao.modelo.ItensEstoque;
import integracao.modelo.RelacaoSaidaMedicamentos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import medicamentos.modelo.Medicamentos;

/**
 *
 * @author rfid
 */
public class IntegracaoDAO {

    private Connection con = null;
    private ArrayList<String> erros = null;

    public IntegracaoDAO() throws SQLException {
        this.con = FabricaConexao.getConnection();
        erros = new ArrayList<>();
    }

    public IntegracaoDAO(Connection con) throws SQLException {
        this.con = con;
        erros = new ArrayList<>();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            Funcoes.registraLogErros(erros);
            // super.finalize();
        } finally {
            super.finalize();
        }

    }

    public ArrayList<Medicamentos> buscarMedicamentos(int numDocumento) throws SQLException {
        ArrayList<Medicamentos> lista = null;
        String sql = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, numDocumento);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                lista = new ArrayList<>();
                ArrayList<String> columns = new ArrayList<>();
                columns = Funcoes.retornaColunas(rsmd, columns);
                while (rs.next()) {
                    try {
                        lista.add(new Medicamentos(rs, columns));
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("#ERRO METODO(buscarMedicamentos): "
                                + e.getMessage());
                        erros.add("#ERRO METODO(buscarMedicamentos): " + e.getMessage());
                    }
                }
            }

        } catch (SQLException e) {
            System.err.println("#ERRO METODO(buscarMedicamentos): " + e.getErrorCode() + " - " + e.getMessage());
            erros.add("#ERRO METODO(buscarMedicamentos): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }

        return lista;
    }

    public ArrayList<RelacaoSaidaMedicamentos> buscarSaidasMedicamentos() throws SQLException {
        ArrayList<RelacaoSaidaMedicamentos> lista = null;
        String sql = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                lista = new ArrayList<>();
                ArrayList<String> columns = new ArrayList<>();
                columns = Funcoes.retornaColunas(rsmd, columns);
                while (rs.next()) {
                    try {
                        lista.add(new RelacaoSaidaMedicamentos(rs, columns));
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("#ERRO METODO(buscarSaidasMedicamentos): posição inválida."
                                + e.getMessage());
                    }
                }
            }

        } catch (SQLException e) {
            System.err.println("#ERRO METODO(buscarSaidasMedicamentos): " + e.getErrorCode() + " - " + e.getMessage());
            erros.add("#ERRO METODO(buscarSaidasMedicamentos): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }

        return lista;
    }

    public void registraSaidaEstoque(List<String> lista, List<String> listaProfissionais) throws SQLException {
        int ok = 1; //1 - falha | 2 - sucesso ao gravar
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sqlRecAberta = null;//buscar os itens das receitas que estão em aberto (ID_RECEITA NULL OU O[ZERO])
        String sqlRecFinaliz = null;//buscar os itens das receitas que estão finalizadas
        String sqlRecContigencia = null;//buscar os itens das receitas que estão em contigencia
        String where;

        ArrayList<ItensEstoque> listaRecAberta = new ArrayList<>();
        ArrayList<ItensEstoque> listaRecFinaliz = new ArrayList<>();
        ArrayList<ItensEstoque> listaRecContigencia = new ArrayList<>();

        try {

            /*ITENS REFERENTES A RECEITAS EM ABERTO*/
            stmt = con.prepareStatement(sqlRecAberta);
            rs = stmt.executeQuery();

            while (rs.next()) {
                if (lista.contains(rs.getString("cod_epc"))) {
                    //this.setMedicamentosAberto();
                    listaRecAberta.add(
                            new ItensEstoque(rs.getString("cod_epc"),
                                    rs.getInt("id_receita"), rs.getInt("id_medicamento"), rs.getInt("id_item"), 0));
                    lista.remove(rs.getString("cod_epc"));
                }
            }
            if (lista.size() > 0) {
                /*ITENS REFERENTES A RECEITAS FINALIZADAS MAS COM ITEM SEM BAIXA*/
                stmt = con.prepareStatement(sqlRecFinaliz);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    if (lista.contains(rs.getString("cod_epc"))) {
                        //this.setMedicamentosAberto();
                        listaRecFinaliz.add(
                                new ItensEstoque(rs.getString("cod_epc"),
                                        rs.getInt("id_receita"), rs.getInt("id_medicamento"), rs.getInt("id_item"), 0));
                        lista.remove(rs.getString("cod_epc"));
                    }
                }
                if (lista.size() > 0) {
                    /*ITENS REFERENTES A RECEITAS FINALIZADAS MAS COM ITEM SEM BAIXA*/
                    if (lista.size() > 0) {
                        where = this.retornaIdsListaItens(lista);
                        where = where != null ? "WHERE IN " + where : where;

                        stmt = con.prepareStatement(sqlRecContigencia + where);//adaptar o where
                        rs = stmt.executeQuery();
                        while (rs.next()) {
                            listaRecContigencia.add(
                                    new ItensEstoque(rs.getString("cod_epc"),
                                            rs.getInt("id_receita"), rs.getInt("id_medicamento"), rs.getInt("id_item"), 0));
                        }
                    }
                }
                this.registraLogSaidaMedicamentos(listaRecAberta, listaRecFinaliz, listaRecContigencia, listaProfissionais);
            }
        } catch (SQLException e) {
            System.err.println("#ERRO METODO(registraSaidaEstoque): " + e.getErrorCode() + " - " + e.getMessage());
            erros.add("#ERRO METODO(registraSaidaEstoque): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }

    }

    public void registraEntradaEstoque(List<String> lista) {
        int ok = 1; //1 - falha | 2 - sucesso ao gravar

    }

    public void registraLogSaidaMedicamentos(ArrayList<ItensEstoque> listaRecAberta, ArrayList<ItensEstoque> listaRecFinaliz,
            ArrayList<ItensEstoque> listaRecContigencia, List<String> listaProfissionais) {
        //lembrar de dar comit no codigo
    }

    /**
     * Gera a condição(WHERE) dos itens que podem ser usados em uma operação SQL
     * receita.
     *
     * @param itensRetirar
     * @param itensLista
     * @return listaIdsItens
     */
    private String retornaIdsListaItens(List<String> itensLista) {
        String listaIdsItens = null;

        if (itensLista.size() > 0) {
            for (int i = 0; i < itensLista.size() - 1; i++) {
                listaIdsItens = listaIdsItens + itensLista.get(i) + ", ";
            }
            listaIdsItens = " (" + listaIdsItens.substring(0, (listaIdsItens.length() - 2)) + ") ";
        }
        return listaIdsItens;
    }
}
