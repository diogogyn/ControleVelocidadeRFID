package integracao.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rfid
 */
public class ItensEstoque {

    protected int idItem;
    protected int idReceita;
    protected int idMedicamento;
    protected int qtde_itens;
    protected String codEpc;

    public ItensEstoque(String codEpc, int idReceita,int idMedicamento, int idItem, int qtde_itens) {
        this.idItem = idItem;
        this.idReceita = idReceita;
        this.idMedicamento = idMedicamento;        
        this.qtde_itens = qtde_itens;
        this.codEpc = codEpc;
        
    }

    public ItensEstoque(ResultSet rs, ArrayList<String> columns) throws SQLException {
        this.idItem = columns.contains("id_item") ? rs.getInt("id_item") : 0;
        this.idReceita = columns.contains("id_receita") ? rs.getInt("id_receita") : 0;
        this.idMedicamento = columns.contains("id_medicamento") ? rs.getInt("id_medicamento") : 0;
        this.qtde_itens = columns.contains("qtde") ? rs.getInt("qtde") : 0;
        this.codEpc = columns.contains("cod_epc") ? rs.getString("cod_epc") : null;
    }

    public int getId_item() {
        return idItem;
    }

    public void setId_item(int idItem) {
        this.idItem = idItem;
    }

    public int getQtde_itens() {
        return qtde_itens;
    }

    public void setQtde_itens(int qtde_itens) {
        this.qtde_itens = qtde_itens;
    }

    public String getCodEpc() {
        return codEpc;
    }

    public void setCodEpc(String codEpc) {
        this.codEpc = codEpc;
    }

}
