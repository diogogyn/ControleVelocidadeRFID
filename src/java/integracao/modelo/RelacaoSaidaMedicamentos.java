/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integracao.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

/**
 *
 * @author rfid
 */
public class RelacaoSaidaMedicamentos {

    protected int protocolo;
    protected String data;
    protected int qtde;
    protected String situacao;

    public RelacaoSaidaMedicamentos() {
        this.protocolo = 0;
        this.data = null;
        this.qtde = 0;
        this.situacao = null;
    }

    public RelacaoSaidaMedicamentos(int protocolo, String data, int qtde, String situacao) {
        this.protocolo = protocolo;
        this.data = data;
        this.qtde = qtde;
        this.situacao = situacao;
    }

    public RelacaoSaidaMedicamentos(HttpServletRequest request) {
        try {
            this.protocolo = request.getParameter("protocolo") != null
                    ? Integer.parseInt(request.getParameter("protocolo")) : 0;
            this.data = request.getParameter("data") != null
                    ? request.getParameter("data") : null;
            this.qtde = request.getParameter("qtde") != null
                    ? Integer.parseInt(request.getParameter("protocolo")) : 0;
            this.situacao = request.getParameter("situacao") != null
                    ? request.getParameter("situacao") : null;
        } catch (HTTPException e) {
            System.err.println("#ERRO (Construtor: RelacaoSaidaMedicamentos): " + e.getMessage());
        }
    }

    public RelacaoSaidaMedicamentos(ResultSet rs, ArrayList<String> columns) throws SQLException {
        try {
            this.protocolo = columns.contains("protocolo") ? rs.getInt("protocolo") : 0;
            this.data = columns.contains("data") ? rs.getString("data") : null;
            this.qtde = columns.contains("qtde") ? rs.getInt("qtde") : 0;
            this.situacao = columns.contains("situacao") ? rs.getString("situacao") : null;
        } catch (SQLException e) {
            System.err.println("#ERRO (Construtor: RelacaoSaidaMedicamentos): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            rs.close();
        }
    }

    public int getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(int protocolo) {
        this.protocolo = protocolo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getQtde() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde = qtde;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

}
