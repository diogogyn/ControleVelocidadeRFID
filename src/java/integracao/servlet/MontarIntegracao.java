package integracao.servlet;

import paciente.servlet.*;
import aplicacao.modelo.Mensagens;
import aplicacao.modelo.Menu;
import conexao.FabricaConexao;
import integracao.dao.IntegracaoDAO;
import integracao.dao.ValidaIntegracao;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import medicamentos.modelo.Medicamentos;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Diogo
 */
public class MontarIntegracao extends Action {

    HttpSession session;

    @Override
    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws SQLException, ParseException {
        session = request.getSession(true);
        //if (session.getAttribute("login") == null) {
        //    return map.findForward("login");
        //} else {
        Menu m = new Menu();
        IntegracaoDAO dao = null;
        boolean btnGerarRelEntrad = false;
        request.setAttribute("menu", m);
        try {
            if (request.getAttribute("listaSaidas") != null) {
                request.setAttribute("listaSaidas", request.getAttribute("listaSaidas"));
            } else {
                dao = new IntegracaoDAO();
                request.setAttribute("listaSaidas", dao.buscarSaidasMedicamentos());
            }
        } catch (Exception err) {
            System.err.println("#ERRO: " + err.getMessage());
        }
        if (request.getParameter("pagina") != null) {
            if (ValidaIntegracao.validaParametros(m, request.getParameter("pagina"))) {
                if (dao == null) {
                    dao = new IntegracaoDAO();
                }
                //editar paciente
                if (request.getParameter("pagina").contains(m.getINT_ENTRADA_MATERIAL())) {
                    //
                    if (ValidaIntegracao.validaParametrosOpcoes(m, request.getParameter("opcao"))) {
                        /*ROTINA PARA BUSCAR PROCEDIMENTOS PARA ENTRADA EM ESTOQUE*/
                        if (request.getParameter("opcao").contains(m.getINT_ENTRADA_MATERIAL())) {
                            if (request.getParameter("num_documento") != null) {
                                request.setAttribute("listaItens", dao.buscarMedicamentos(Integer.parseInt(request.getParameter("num_documento"))));
                                btnGerarRelEntrad = true;
                            }
                        }
                    }
                }
            }
        }
        request.setAttribute("btnGerarRelEntrad", btnGerarRelEntrad);
        return map.findForward("fwdIntEntradaMaterial");
        //}
    }
}
