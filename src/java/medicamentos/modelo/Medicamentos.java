package medicamentos.modelo;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import receita.modelo.ReceitasPrescritas;
import aplicacao.dao.Funcoes;
import java.sql.SQLException;

import java.util.ArrayList;

/**
 *
 * @author Diogo
 */
public class Medicamentos extends ReceitasPrescritas {

    private int numDanfe;
    private int codigoMedicamento;
    private String lote;
    private String unidEntrada;
    
    public Medicamentos() {
        this.numDanfe = 0;
        this.codigoMedicamento = 0;
        this.lote = null;
        this.unidEntrada = null;
        this.unidSaida = null;
        this.area = 0;
        this.rua = 0;
        this.modulo = 0;
        this.nivel = 0;
        this.vao = 0;

        dtValidade = null;
        status = null;
        idReceita = 0;
        nomeMedicamento = null;
        numReceita = null;
        prescritor = null;
        qtdeTotal = 0;
    }

    public Medicamentos(HttpServletRequest request) {
                
        this.numDanfe = request.getParameter("num_danfe") != null ? Integer.parseInt(request.getParameter("num_danfe")) : 0;
        this.codigoMedicamento = request.getParameter("cod_medicamento") != null ? Integer.parseInt(request.getParameter("cod_medicamento")) : 0;
        this.lote = request.getParameter("lote") != null ? request.getParameter("lote") : null;
        this.unidEntrada = request.getParameter("unid_entrada") != null ? request.getParameter("unid_entrada") : null;
        this.unidSaida = request.getParameter("unid_saida") != null ? request.getParameter("unid_saida") : null;
        this.area = request.getParameter("area") != null ? Integer.parseInt(request.getParameter("area")) : 0;
        this.rua = request.getParameter("rua") != null ? Integer.parseInt(request.getParameter("rua")) : 0;
        this.modulo = request.getParameter("modulo") != null ? Integer.parseInt(request.getParameter("modulo")) : 0;
        this.nivel = request.getParameter("nivel") != null ? Integer.parseInt(request.getParameter("nivel")) : 0;
        this.vao = request.getParameter("vao") != null ? Integer.parseInt(request.getParameter("vao")) : 0;

        dtValidade = request.getParameter("dtValidade") != null ? request.getParameter("pagina") : null;
        status = request.getParameter("status") != null ? request.getParameter("status") : null;
        idReceita = request.getParameter("id_receita") != null ? Integer.parseInt(request.getParameter("id_receita")) : 0;
        nomeMedicamento = request.getParameter("nome_medicamento") != null ? request.getParameter("nome_medicamento") : null;
        numReceita = request.getParameter("num_receita") != null ? request.getParameter("num_receita") : null;
        prescritor = request.getParameter("prescritor") != null ? request.getParameter("prescritor") : null;
        qtdeTotal = request.getParameter("qtde_total") != null ? Integer.parseInt(request.getParameter("qtde_total")) : 0;
    }

    public Medicamentos(ResultSet rs, ArrayList<String> columns) throws SQLException {

        try {
            this.numDanfe = columns.contains("num_danfe") ? rs.getInt("num_danfe") : 0;
            this.codigoMedicamento = columns.contains("cod_medicamento") ? rs.getInt("cod_medicamento") : 0;
            this.lote = columns.contains("lote") ? rs.getString("lote") : null;
            this.unidEntrada = columns.contains("unid_entrada") ? rs.getString("unid_entrada") : null;
            this.unidSaida = columns.contains("unid_saida") ? rs.getString("unid_saida") : null;
            this.area = columns.contains("area") ? rs.getInt("area") : 0;
            this.rua = columns.contains("rua") ? rs.getInt("rua") : 0;
            this.modulo = columns.contains("modulo") ? rs.getInt("modulo") : 0;
            this.nivel = columns.contains("nivel") ? rs.getInt("nivel") : 0;
            this.vao = columns.contains("vao") ? rs.getInt("vao") : 0;

            dtValidade = columns.contains("dt_validade") ? rs.getString("dt_validade") : null;
            status = columns.contains("status") ? rs.getString("status") : null;
            idReceita = columns.contains("id_receita") ? rs.getInt("id_receita") : 0;
            nomeMedicamento = columns.contains("nome_medicamento") ? rs.getString("nome_medicamento") : null;
            numReceita = columns.contains("num_receita") ? rs.getString("num_receita") : null;
            prescritor = columns.contains("prescritor") ? rs.getString("prescritor") : null;
            qtdeTotal = columns.contains("qtde_total") ? rs.getInt("qtde_total") : 0;
        } catch (SQLException e) {
            System.err.println("#ERRO (Construtor: Medicamentos): "+e.getErrorCode() +" - "+e.getMessage());
        }finally{
            rs.close();
        }
    }

    public int getNumDanfe() {
        return numDanfe;
    }

    public void setNumDanfe(int numDanfe) {
        this.numDanfe = numDanfe;
    }

    public int getCodigoMedicamento() {
        return codigoMedicamento;
    }

    public void setCodigoMedicamento(int codigoMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getUnidEntrada() {
        return unidEntrada;
    }

    public void setUnidEntrada(String unidEntrada) {
        this.unidEntrada = unidEntrada;
    }

    public String getUnidSaida() {
        return unidSaida;
    }

    public void setUnidSaida(String unidSaida) {
        this.unidSaida = unidSaida;
    }
}
