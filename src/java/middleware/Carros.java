/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package middleware;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author solange
 */
public class Carros {

    private Date momentoPassagem;
    private String hashCode;//refere-se ao codigo EPC da tag
    
    Carros(Date date) {
        this.momentoPassagem = date;
    }

    Carros(Date date, String hashcode) {
        this.momentoPassagem = date;
        this.hashCode = hashcode;
    }

    Carros(String hashcode) {
        this.hashCode = hashcode;
    }
    
    public Date getMomentoPassagem(){
        return momentoPassagem;
    }
    
    public String getHashCode(){
        return hashCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.hashCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carros other = (Carros) obj;
        if (!Objects.equals(this.hashCode, other.hashCode)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
