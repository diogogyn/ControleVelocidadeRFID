/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleware;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author solange
 */
@Entity
@Table(name = "EPCs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EPCs.findAll", query = "SELECT e FROM EPCs e"),
    @NamedQuery(name = "EPCs.findById", query = "SELECT e FROM EPCs e WHERE e.id = :id"),
    @NamedQuery(name = "EPCs.findByEpc", query = "SELECT e FROM EPCs e WHERE e.epc = :epc"),
    @NamedQuery(name = "EPCs.findByHorario1", query = "SELECT e FROM EPCs e WHERE e.horario1 = :horario1"),
    @NamedQuery(name = "EPCs.findByHorario2", query = "SELECT e FROM EPCs e WHERE e.horario2 = :horario2")})
public class EPCs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "epc")
    private String epc;
    @Column(name = "horario1")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horario1;
    @Column(name = "horario2")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horario2;

    public EPCs() {
    }

    public EPCs(String epc, Date horario1, Date horario2) {
        this.epc = epc;
        this.horario1 = horario1;
        this.horario2 = horario2;
    }
    
    public EPCs(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEpc() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }

    public Date getHorario1() {
        return horario1;
    }

    public void setHorario1(Date horario1) {
        this.horario1 = horario1;
    }

    public Date getHorario2() {
        return horario2;
    }

    public void setHorario2(Date horario2) {
        this.horario2 = horario2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EPCs)) {
            return false;
        }
        EPCs other = (EPCs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "middleware.EPCs[ id=" + id + " ]";
    }

}
