/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleware;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.hibernate.TransactionException;

/**
 *
 * @author solange
 */
public class EnviaLista extends java.lang.Thread {

    // área de dados da Thread
    Middleware_LLRPv2 lista;
    Set<EPCs> listaItensBD = new HashSet<>();//PARA ARMAZENAR OS DADOS DO BD PARA ENVIO

    public EnviaLista(Middleware_LLRPv2 listaMateriais) {
        lista = listaMateriais;
    }

    public void run() {
        while (true) {
            try {
                System.out.println("Thread 3 - Envio lista");
                Thread.sleep(100); // coloca a "thread" para "dormir" 1 segundo

                lista.semaphore.acquire();

                if (lista.listaItensParaEnvio.size() > 0) {
                    Thread.sleep(10000); // PARA SIMULAR O ENVIO DOS DADOS
                    if (true) {//testar conexão com serv de processamento
                        /*EntityManagerFactory factory = Persistence.createEntityManagerFactory("tarefas");
                        EntityManager manager = factory.createEntityManager();
                        //1 - PRIMEIRO ENVIAR OS DADOS SALVOS NO BD(listaItensBD)
                        
                        //2 - EXCLUIR OS ITENS DO BD QUE FORAM ENVIADOS COM SUCESSO
                        for (EPCs list : listaItensBD) {
                            manager.getTransaction().begin();
                            manager.remove(list);
                            manager.getTransaction().commit();
                        }
                                */
                        //3 - ENVIAR OS ITENS salvo em memoria (lista.listaItensParaEnvio)
                        //4 - LIMPAR LISTA salvo em memoria(lista.listaItensParaEnvio)
                        //4 - LIMPAR LISTA DE ITENS PARA ENVIO(listaItensBD)

                    } else {
                        /*EntityManagerFactory factory = Persistence.createEntityManagerFactory("tarefas");
                        EntityManager manager = factory.createEntityManager();
                        for (EPCs list : lista.listaItensParaEnvio) {
                            try {
                                manager.getTransaction().begin();
                                manager.persist(list);
                                manager.getTransaction().commit();

                                manager.remove(list);
                                manager.getTransaction().commit();
                                lista.listaItensParaEnvio.remove(list);
                            } catch (TransactionException t) {
                                //TODO
                            } catch (Exception e) {

                            }
                        }*/
                        //1 - SALVAR REGISTROS A SEREM ENVIADOS NO BD
                        //2 - SALVAR LOG DE FALHA DE ENVIO.
                        //3 - LIMPAR LISTA DE ITENS PARA ENVIO
                    }
                    //
                    System.out.println("Thread 3 - ENVIOU " + lista.listaItensParaEnvio.size() + " itens.");
                    lista.listaItensParaEnvio.clear();
                } else {
                    System.out.println("Thread 3 - NAO ENVIOU");
                    if (true) {//testar conexão com serv de processamento
                        //1 - ENVIAR OS DADOS SALVOS NO BD
                        //2 - EXCLUIR OS ITENS DO BD QUE FORAM ENVIADOS COM SUCESSO
                    }
                }
                lista.semaphore.release();
            } catch (InterruptedException ex) {
                Logger.getLogger(EnviaLista.class.getName()).log(Level.SEVERE, null, ex);
            }
            //System.out.println("Lista de envio limpa");
        }

    }

    public boolean persisteDados() {

        return true;
    }
}
