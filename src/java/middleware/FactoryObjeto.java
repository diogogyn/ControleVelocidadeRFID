/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package middleware;

import middleware.Carros;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author solange
 */
class FactoryObjeto {
    
    
    static Carros getValue(String hashcode) {
        return new Carros(new Date(), hashcode);
    }

    static Carros getValue() {
        return new Carros(new Date());
    }

    static Set<Carros> obterObjetoResultante(Set<Carros> listRFID_ini, Set<Carros> listRFID_fim, String hascode) {
        
        List list1 = new ArrayList<>(listRFID_ini);
        Carros interesse = new Carros(hascode);
        
        if(listRFID_ini.contains(interesse) && listRFID_fim.contains(interesse)){
            
            Carros a = (Carros)list1.get(list1.indexOf(new Carros(new Date(), hascode)));

            List list2 = new ArrayList<>(listRFID_fim);
            Carros b = (Carros)list2.remove(list2.indexOf(a));
            
        }
        
        return null;       
    }
    
}
