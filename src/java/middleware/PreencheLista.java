package middleware;

import java.util.ArrayList;
import java.util.List;

public class PreencheLista extends java.lang.Thread {

    // área de dados da Thread
    Middleware_LLRP lista1;

    public PreencheLista(Middleware_LLRP listaMateriais) {
        lista1 = listaMateriais;
    }
    Middleware_LLRPv2 lista;

    public PreencheLista(Middleware_LLRPv2 listaMateriais) {
        lista = listaMateriais;
    }

    // área de código da Thread
    public void run() {
        while (true) {
            //Se a lista estiver sendo processada na outra thread, então esperar para atualizar no outro ciclo;
            /*if (lista.realizarAtualizacaoEstoque == 1) {
                try {
                    Thread.sleep(3000); // coloca a "thread" para "ninar" 1 segundo
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
            }*/
            //Realiza o processo de atualização de estoque
            try {
                Thread.sleep(1500); // coloca a "thread" para "dormir" 1 segundo
                //esta variavel é usada para estabeler comunicação entre os processos concorrentes.
                lista.semaphore.acquire();
                
                //lista.realizarAtualizacaoEstoque = 1;
                System.out.println("Thread 2 - Preenchendo lista");
                
                for (Carros listAntena1 : lista.listaItensEPCAntena1) {
                    //System.out.println("fazendo a interacao");
                    List list1 = new ArrayList<>(lista.listaItensEPCAntena1);
                    Carros interesse = new Carros(listAntena1.getHashCode());

                    if (lista.listaItensEPCAntena1.contains(interesse) && lista.listaItensEPCAntena2.contains(interesse)) {
                        //System.out.println("Item esta nas listas");
                        Carros a = (Carros) list1.remove(list1.indexOf(new Carros(listAntena1.getHashCode())));
                        //lista.listaItensEPCAntena1.remove(a);

                        List list2 = new ArrayList<>(lista.listaItensEPCAntena2);
                        Carros b = (Carros) list2.remove(list2.indexOf(a));
                        //lista.listaItensEPCAntena2.remove(b);

                        lista.listaItensParaEnvio.add(
                                new EPCs(a.getHashCode(),/*EPC*/
                                        a.getMomentoPassagem(),/*HORARIO 1ª LEITURA*/
                                        b.getMomentoPassagem()));/*HORARIO 2ª LEITURA*/

                        //System.out.println("Veiculo: " + a.getHashCode() + " entre " + a.getMomentoPassagem() + " e " + b.getMomentoPassagem());
                    }
                }
                System.out.println("Thread 2 - "+lista.listaItensParaEnvio.size()+" itens");
                //lista.realizarAtualizacaoEstoque = 0;
                lista.semaphore.release();
                
            } catch (InterruptedException e) {
                e.printStackTrace(System.err);
            }
        }
    }
}
