package middleware;

import conexao.FabricaConexao;
import integracao.dao.IntegracaoDAO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PreencheLista1 extends java.lang.Thread {

    // área de dados da Thread
    Middleware_LLRP lista;
    String dadosParaEnvio = "";
    private Connection con = null;
    private IntegracaoDAO dao;

    public PreencheLista1(Middleware_LLRP listaMateriais) throws SQLException {
        lista = listaMateriais;
        try {
            this.con = FabricaConexao.getConnection();
            this.dao = new IntegracaoDAO(con);
        } catch (SQLException e) {
            System.exit(0);
        }
    }

    public PreencheLista1(Middleware_LLRP listaMateriais, Connection con) throws SQLException {
        lista = listaMateriais;
        this.con = con;
        this.dao = new IntegracaoDAO(con);
    }

    // área de código da Thread
    public void run() {
        while (true) {
            //Se a lista estiver sendo processada na outra thread, então esperar para atualizar no outro ciclo;
            if (lista.realizarAtualizacaoEstoque == 1) {
                try {
                    Thread.sleep(3000); // coloca a "thread" para "ninar" 1 segundo
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
            }
            //Realiza o processo de atualização de estoque
            try {
                Thread.sleep(15000); // coloca a "thread" para "dormir" 1 segundo
                if (!con.isClosed()) {///verifica se a conexão esta valida
                    System.out.println("Processando os dados!!");
                    //esta variavel é usada para estabeler comunicação entre os processos concorrentes.
                    lista.realizarAtualizacaoEstoque = 1;
                    System.out.println("Qtde antena 1(ANTES): "+lista.listaItensEPCAntena1.size());
                    System.out.println("Qtde antena 2(ANTES): "+lista.listaItensEPCAntena2.size());
                    for (Carros listAntena1 : lista.listaItensEPCAntena1) {
                        //System.out.println("fazendo a interacao");
                        List list1 = new ArrayList<>(lista.listaItensEPCAntena1);
                        Carros interesse = new Carros(listAntena1.getHashCode());

                        if (lista.listaItensEPCAntena1.contains(interesse) && lista.listaItensEPCAntena2.contains(interesse)) {
                            //System.out.println("Item esta nas listas");
                            Carros a = (Carros) list1.remove(list1.indexOf(new Carros(listAntena1.getHashCode())));
                            lista.listaItensEPCAntena1.remove(a);
                            
                            List list2 = new ArrayList<>(lista.listaItensEPCAntena2);
                            Carros b = (Carros) list2.remove(list2.indexOf(a));
                            lista.listaItensEPCAntena2.remove(b);
                            
                            lista.listaItensParaEnvio.add(
                                    new EPCs(a.getHashCode(),/*EPC*/
                                            a.getMomentoPassagem(),/*HORARIO 1ª LEITURA*/
                                            b.getMomentoPassagem()));/*HORARIO 2ª LEITURA*/

                            //System.out.println("Veiculo: " + a.getHashCode() + " entre " + a.getMomentoPassagem() + " e " + b.getMomentoPassagem());
                        }
                    }
                    //dao.realizarEnvioDados(this);
                    if (lista.listaItensParaEnvio.size() > 0) {
                        System.out.println("QTDE a enviar: " + lista.listaItensParaEnvio.size());
                        enviaDadosServidorProcessamento();
                        System.out.println("Qtde antena 1(DEPOIS): "+lista.listaItensEPCAntena1.size());
                        System.out.println("Qtde antena 2(DEPOIS): "+lista.listaItensEPCAntena2.size());
                    
                    } else {
                        System.out.println("Nenhum registro para ser enviado!");
                    }
                }

                lista.realizarAtualizacaoEstoque = 0;
            } catch (InterruptedException e) {
                e.printStackTrace(System.err);
            } catch (SQLException ex) {
                Logger.getLogger(PreencheLista1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void processaDadosParaEnvio() {
        for (EPCs list : lista.listaItensParaEnvio) {
            dadosParaEnvio = dadosParaEnvio + list.getEpc() + "|" + list.getHorario1() + "|" + list.getHorario2() + ";\n";
        }
    }

    public boolean enviaDadosServidorProcessamento() {
        //criar algorimo para conexao com servidor e envio dos dados
        //for (EPCs list : lista.listaItensParaEnvio) {
        //    System.out.println("Veiculo: " + list.getEpc() + " entre " + list.getHorario1() + " e " + list.getHorario2());
        //}
        //System.out.println("----------------------------------------------------");
        //System.out.println("Token a ser enviado: ");
        //System.out.println(dadosParaEnvio);
        lista.listaItensParaEnvio.clear();
        return true;
    }
}
