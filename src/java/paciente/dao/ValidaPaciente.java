package paciente.dao;

import aplicacao.dao.Funcoes;
import aplicacao.modelo.Mensagens;
import aplicacao.modelo.Menu;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rfid
 */
public class ValidaPaciente {

    public static boolean validaParametros(Menu m, String param) {
        if (param != null) {
            if (param.equals(m.getCAD_ADD_PACIENTE())) {
                return true;
            } else return param.equals(m.getCAD_EDT_PACIENTE());
        } else {
            return false;
        }
    }

    public static Mensagens validaCamposDadosPessoais(HttpServletRequest request) {
        Mensagens mens = new Mensagens();
        if (request.getParameter("nome") == null) {
            return mens;
        } else if (request.getParameter("nome_mae") == null) {
            return mens;
        } else if (Funcoes.validaData(request.getParameter("dt_nascimento"))) {
            return mens;
        } else if (request.getParameter("sexo") == null) {
            return mens;
        } else if (request.getParameter("logradouro") == null) {
            return mens;
        } else if (request.getParameter("estado") == null) {
            return mens;
        } else if (request.getParameter("cidade") == null) {
            return mens;
        } else if (request.getParameter("bairro") == null) {
            return mens;
        } else if (Funcoes.validaCPF(request.getParameter("cpf"))) {
            return mens;
        } else if (request.getParameter("telefone") == null) {
            return mens;
        }
        return mens;
    }

    public static String validaCamposPlanosSaude(HttpServletRequest request) {
        if (request.getParameter("num_beneficio") == null) {
            return "";
        } else if (Funcoes.validaData(request.getParameter("dt_validade"))) {
            return "";
        }
        return null;
    }
}
