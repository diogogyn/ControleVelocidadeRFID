package paciente.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

/**
 *
 * @author Diogo
 */
public class PacAtencaoContinuada extends Paciente {

    private int idAtencContinuada;
    private String nomeAtencao;

    public PacAtencaoContinuada() {
        this.idAtencContinuada = 0;
        this.nomeAtencao = null;
    }

    public PacAtencaoContinuada(int idAtencContinuada, String nomeAtencao) {
        this.idAtencContinuada = idAtencContinuada;
        this.nomeAtencao = nomeAtencao;
    }

    public PacAtencaoContinuada(HttpServletRequest request) {
        try {
            this.idAtencContinuada = request.getParameter("id_atenc_continuada") != null ? Integer.parseInt(request.getParameter("id_atenc_continuada")) : 0;
            this.nomeAtencao = request.getParameter("nome_atencao") != null ? request.getParameter("nome_atencao") : null;
        } catch (HTTPException e) {
            System.err.println("#ERRO: CONSTRUTOR(PacAtencaoContinuada): " + e.getMessage());
        }catch (Exception e) {
            System.err.println("#ERRO: CONSTRUTOR(PacAtencaoContinuada): " + e.getMessage());
        }
    }

    public PacAtencaoContinuada(ResultSet rs) throws SQLException {
        try {
            this.idAtencContinuada = rs.getInt("id_atenc_continuada");
            this.nomeAtencao = rs.getString("nome_atencao");
        } catch (SQLException e) {
            System.err.println("#ERRO (Construtor: PacAtencaoContinuada): " + e.getErrorCode() + " - " + e.getMessage());
        }catch (Exception e) {
            System.err.println("#ERRO: CONSTRUTOR(PacAtencaoContinuada): " + e.getMessage());
        } finally {
            rs.close();
        }
    }

    public int getIdAtencContinuada() {
        return idAtencContinuada;
    }

    public void setIdAtencContinuada(int idAtencContinuada) {
        this.idAtencContinuada = idAtencContinuada;
    }

    public String getNomeAtencao() {
        return nomeAtencao;
    }

    public void setNomeAtencao(String nomeAtencao) {
        this.nomeAtencao = nomeAtencao;
    }

}
