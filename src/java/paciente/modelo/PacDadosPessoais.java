package paciente.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

/**
 *
 * @author Diogo
 */
public class PacDadosPessoais extends Paciente {

    private String nome;
    private String nomeMae;
    private String dataNascimento;//pesquisar nova classe para data
    private String sexo;
    private String logradouro;
    private String complemento;
    private String estado;
    private String cidade;
    private String bairro;
    private String situacao;
    private String cpf;
    private String telefone;

    /**
     *
     */
    public PacDadosPessoais() {
        this.id_paciente = 0;
        this.nome = null;
        this.nomeMae = null;
        this.dataNascimento = null;
        this.sexo = null;
        this.logradouro = null;
        this.complemento = null;
        this.estado = null;
        this.cidade = null;
        this.bairro = null;
        this.situacao = null;
        this.cpf = null;
        this.telefone = null;
    }

    public PacDadosPessoais(HttpServletRequest request) {
        try {
            this.id_paciente = request.getParameter("id_paciente") != null ? Integer.parseInt(request.getParameter("id_paciente")) : 0;
            this.nome = request.getParameter("nome") != null ? request.getParameter("nome") : null;
            this.nomeMae = request.getParameter("nome_mae") != null ? request.getParameter("nome_mae") : null;
            this.dataNascimento = request.getParameter("dt_nascimento") != null ? request.getParameter("dt_nascimento") : null;
            this.sexo = request.getParameter("sexo") != null ? request.getParameter("sexo") : null;
            this.logradouro = request.getParameter("logradouro") != null ? request.getParameter("logradouro") : null;
            this.complemento = request.getParameter("complemento") != null ? request.getParameter("complemento") : null;
            this.estado = request.getParameter("estado") != null ? request.getParameter("estado") : null;
            this.cidade = request.getParameter("cidade") != null ? request.getParameter("cidade") : null;
            this.bairro = request.getParameter("bairro") != null ? request.getParameter("bairro") : null;
            this.situacao = request.getParameter("situacao") != null ? request.getParameter("situacao") : null;
            this.cpf = request.getParameter("cpf") != null ? request.getParameter("cpf") : null;
            this.telefone = request.getParameter("telefone") != null ? request.getParameter("telefone") : null;
        } catch (HTTPException e) {
            System.err.println("#ERRO: CONSTRUTOR(PacDadosPessoais): " + e.getMessage());
        }
    }

    public PacDadosPessoais(ResultSet rs, ArrayList<String> columns) throws SQLException {
        try {
            this.id_paciente = columns.contains("id_paciente") ? rs.getInt("id_paciente") : 0;
            this.nome = columns.contains("nome") ? rs.getString("nome") : null;
            this.nomeMae = columns.contains("nome_mae") ? rs.getString("nome_mae") : null;
            this.dataNascimento = columns.contains("dt_nascimento") ? rs.getString("dt_nascimento") : null;
            this.sexo = columns.contains("sexo") ? rs.getString("sexo") : null;
            this.logradouro = columns.contains("logradouro") ? rs.getString("logradouro") : null;
            this.complemento = columns.contains("complemento") ? rs.getString("complemento") : null;
            this.estado = columns.contains("estado") ? rs.getString("estado") : null;
            this.cidade = columns.contains("cidade") ? rs.getString("cidade") : null;
            this.bairro = columns.contains("bairro") ? rs.getString("bairro") : null;
            this.situacao = columns.contains("situacao") ? rs.getString("situacao") : null;
            this.cpf = columns.contains("cpf") ? rs.getString("cpf") : null;
            this.telefone = columns.contains("telefone") ? rs.getString("telefone") : null;
        } catch (SQLException e) {
            System.err.println("#ERRO (Construtor: RelacaoSaidaMedicamentos): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            rs.close();
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

}
