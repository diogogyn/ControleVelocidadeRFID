package paciente.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

/**
 *
 * @author Diogo
 */
public class PacPlanosSaude extends Paciente {

    private int idPlanoSaude;
    private String nomePlano;
    private String dtValidade;//trocar para data
    private String numBeneficio;

    public PacPlanosSaude() {
        this.idPlanoSaude = 0;
        this.nomePlano = null;
        this.dtValidade = null;
        this.numBeneficio = null;
    }

    public PacPlanosSaude(HttpServletRequest request) {
        try {
            this.idPlanoSaude = request.getParameter("id_plano_saude") != null ? Integer.parseInt(request.getParameter("id_plano_saude")) : 0;
            this.nomePlano = request.getParameter("nome_plano") != null ? request.getParameter("nome_plano") : null;
            this.dtValidade = request.getParameter("dt_validade") != null ? request.getParameter("dt_validade") : null;
            this.numBeneficio = request.getParameter("num_beneficio") != null ? request.getParameter("num_beneficio") : null;
        } catch (HTTPException e) {
            System.err.println("#ERRO: CONSTRUTOR(PacPlanosSaude): " + e.getMessage());
        }catch (Exception e) {
            System.err.println("#ERRO: CONSTRUTOR(PacPlanosSaude): " + e.getMessage());
        }
    }

    public PacPlanosSaude(ResultSet rs, ArrayList<String> columns) throws SQLException {
        try {
            this.idPlanoSaude = columns.contains("id_plano_saude") ? rs.getInt("id_plano_saude") : 0;
            this.nomePlano = columns.contains("nome_plano") ? rs.getString("nome_plano") : null;
            this.dtValidade = columns.contains("dt_validade") ? rs.getString("dt_validade") : null;
            this.numBeneficio = columns.contains("num_beneficio") ? rs.getString("num_beneficio") : null;
        } catch (SQLException e) {
            System.err.println("#ERRO: CONSTRUTOR(PacPlanosSaude): " + e.getMessage());
        } catch (Exception e) {
            System.err.println("#ERRO: CONSTRUTOR(PacPlanosSaude): " + e.getMessage());
        }
    }

    public int getIdPlanoSaude() {
        return idPlanoSaude;
    }

    public void setIdPlanoSaude(int idPlanoSaude) {
        this.idPlanoSaude = idPlanoSaude;
    }

    public String getNomePlano() {
        return nomePlano;
    }

    public void setNomePlano(String nomePlano) {
        this.nomePlano = nomePlano;
    }

    public String getDtValidade() {
        return dtValidade;
    }

    public void setDtValidade(String dtValidade) {
        this.dtValidade = dtValidade;
    }

    public String getNumBeneficio() {
        return numBeneficio;
    }

    public void setNumBeneficio(String numBeneficio) {
        this.numBeneficio = numBeneficio;
    }

}
