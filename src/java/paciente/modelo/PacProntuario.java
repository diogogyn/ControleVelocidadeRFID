package paciente.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

/**
 *
 * @author Diogo
 */
public class PacProntuario extends Paciente {

    private int idReceita;
    private String numReceita;
    private String prescritor;
    private String status;
    private String dtEmissao;//trocar para data
    private int qtdeItens;

    public PacProntuario() {
    }

    public PacProntuario(int idReceita, String numReceita, String prescritor, String status, String dtEmissao, int qtdeItens, ResultSet rs, ArrayList<String> columns) {

        this.idReceita = idReceita;
        this.numReceita = numReceita;
        this.prescritor = prescritor;
        this.status = status;
        this.dtEmissao = dtEmissao;
        this.qtdeItens = qtdeItens;
    }

    /**
     * Cntrutor para CRUD de Select
     *
     * @param rs
     * @param columns
     * @throws SQLException
     */
    public PacProntuario(ResultSet rs, ArrayList<String> columns) throws SQLException {
        try {
            this.idReceita = columns.contains("id_receita") ? rs.getInt("id_receita") : 0;
            this.numReceita = columns.contains("num_receita") ? rs.getString("num_receita") : null;
            this.prescritor = columns.contains("prescritor") ? rs.getString("prescritor") : null;
            this.status = columns.contains("status") ? rs.getString("status") : null;
            this.dtEmissao = columns.contains("dt_emissao") ? rs.getString("dt_emissao") : null;
            this.qtdeItens = columns.contains("qtde_itens") ? rs.getInt("qtde_itens") : 0;
        } catch (SQLException e) {
            System.err.println("#ERRO (Construtor: PacProntuario): " + e.getErrorCode() + " - " + e.getMessage());
        }catch (Exception e) {
            System.err.println("#ERRO: CONSTRUTOR(PacProntuario): " + e.getMessage());
        } finally {
            rs.close();
        }
    }

    /**
     * Construtor para CRUD de INSERT e UPDATE
     *
     * @param request
     */
    public PacProntuario(HttpServletRequest request) {
        try {
            this.idReceita = request.getParameter("id_receita") != null ? Integer.parseInt(request.getParameter("id_receita")) : 0;
            this.numReceita = request.getParameter("num_receita") != null ? request.getParameter("num_receita") : null;
            this.prescritor = request.getParameter("prescritor") != null ? request.getParameter("prescritor") : null;
            this.status = request.getParameter("status") != null ? request.getParameter("status") : null;
            this.dtEmissao = request.getParameter("dt_emissao") != null ? request.getParameter("dt_emissao") : null;
            this.qtdeItens = request.getParameter("qtde_itens") != null ? Integer.parseInt(request.getParameter("qtde_itens")) : 0;
        }catch (HTTPException e) {
            System.err.println("#ERRO: CONSTRUTOR(PacProntuario): " + e.getMessage());
        }catch (Exception e) {
            System.err.println("#ERRO: CONSTRUTOR(PacProntuario): " + e.getMessage());
        } 
    }

    public int getIdReceita() {
        return idReceita;
    }

    public void setIdReceita(int idReceita) {
        this.idReceita = idReceita;
    }

    public String getNumReceita() {
        return numReceita;
    }

    public void setNumReceita(String numReceita) {
        this.numReceita = numReceita;
    }

    public String getPrescritor() {
        return prescritor;
    }

    public void setPrescritor(String prescritor) {
        this.prescritor = prescritor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(String dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public int getQtdeItens() {
        return qtdeItens;
    }

    public void setQtdeItens(int qtdeItens) {
        this.qtdeItens = qtdeItens;
    }

}
