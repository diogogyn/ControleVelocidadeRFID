package paciente.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Diogo
 */
public class Paciente {
    
    protected int id_paciente;
    protected String numProntuario; 
    private PacDadosPessoais pacDadosPessoais;
    private PacPlanosSaude pacPlanosSaude;
    private PacAtencaoContinuada pacAtencaoContinuada;
    ResultSet rs;
    ArrayList<String> columns;

    public Paciente() {
        
    }

    public Paciente(PacDadosPessoais pacDadosPessoais, PacProntuario pacProntuario, PacPlanosSaude pacPlanosSaude, PacAtencaoContinuada pacAtencaoContinuada) {
        this.pacDadosPessoais = pacDadosPessoais;
        this.pacPlanosSaude = pacPlanosSaude;
        this.pacAtencaoContinuada = pacAtencaoContinuada;
    }
    
    public Paciente(HttpServletRequest request) {
        this.pacDadosPessoais = new PacDadosPessoais(request);
        this.pacPlanosSaude =  new PacPlanosSaude(request);
        this.pacAtencaoContinuada =  new PacAtencaoContinuada(request);
    }
    
    public int getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(int id_paciente) {
        this.id_paciente = id_paciente;
    }

    public PacDadosPessoais getPacDadosPessoais() {
        return pacDadosPessoais;
    }

    public void setPacDadosPessoais(PacDadosPessoais pacDadosPessoais) {
        this.pacDadosPessoais = pacDadosPessoais;
    }

    public PacPlanosSaude getPacPlanosSaude() {
        return pacPlanosSaude;
    }

    public void setPacPlanosSaude(PacPlanosSaude pacPlanosSaude) {
        this.pacPlanosSaude = pacPlanosSaude;
    }

    public PacAtencaoContinuada getPacAtencaoContinuada() {
        return pacAtencaoContinuada;
    }

    public void setPacAtencaoContinuada(PacAtencaoContinuada pacAtencaoContinuada) {
        this.pacAtencaoContinuada = pacAtencaoContinuada;
    }
    
    
       
}
