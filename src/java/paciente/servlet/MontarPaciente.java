package paciente.servlet;

import aplicacao.modelo.Mensagens;
import aplicacao.modelo.Menu;
import conexao.FabricaConexao;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import paciente.dao.PacienteDAO;
import paciente.dao.ValidaPaciente;
import paciente.modelo.Paciente;

/**
 *
 * @author Diogo
 */
public class MontarPaciente extends Action {

    HttpSession session;

    @Override
    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws SQLException, ParseException {
        session = request.getSession(true);
        //if (session.getAttribute("login") == null) {
        //    return map.findForward("login");
        //} else {
        Menu m = new Menu();
        Mensagens mens = null;
        request.setAttribute("menu", m);
        if (request.getParameter("pagina") != null) {
            if (ValidaPaciente.validaParametros(m, request.getParameter("pagina"))) {

                //editar paciente
                if (request.getParameter("pagina").contains(m.getCAD_EDT_PACIENTE())) {
                    //BUSCAR INFORMAÇEÕS DO PACIENTE

                    PacienteDAO dao = new PacienteDAO();
                    request.setAttribute("dados", dao.buscaPaciente(Integer.parseInt(request.getParameter("id_paciente"))));
                    request.setAttribute("opcao", "editar");
                    return map.findForward("fwdCadAddPaciente");
                }
                //cadastrar novo paciente
                if (request.getParameter("pagina").contains(m.getCAD_ADD_PACIENTE())) {
                    if (!request.getParameter("opcao").contains("")) {
                        mens = ValidaPaciente.validaCamposDadosPessoais(request);
                        if (mens.ok) {//se falso - ERRO
                            //executa crud
                            Paciente pac = new Paciente(request);
                            PacienteDAO dao = new PacienteDAO(FabricaConexao.getConnection());
                            if (request.getParameter("opcao").contains("adicionar")) {
                                mens = dao.adicionaUsuario(pac);

                                request.setAttribute("opcao", mens.ok ? "editar" : "adicionar");
                            } else if (request.getParameter("opcao").contains("editar")) {
                                mens = dao.editaUsuario(pac);//editar
                            }
                            request.setAttribute("dados", pac);
                        }
                    }
                    request.setAttribute("msg", mens);
                    return map.findForward("fwdCadAddPaciente");
                }
            }
        }
        return map.findForward("fwdCadPaciente");
        //}
    }
}
