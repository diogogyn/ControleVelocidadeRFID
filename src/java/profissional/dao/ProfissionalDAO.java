package profissional.dao;

import aplicacao.modelo.Mensagens;
import conexao.FabricaConexao;
import java.sql.Connection;
import java.sql.SQLException;
import profissional.modelo.Profissional;

/**
 *
 * @author Diogo
 */
public class ProfissionalDAO {
    private Connection con;

    public ProfissionalDAO() throws SQLException {
        this.con = FabricaConexao.getConnection();
    }

    public ProfissionalDAO(Connection con) {
        this.con = con;
    }
    
    public Mensagens adicionaProfissional(Profissional prof){
        Mensagens mens = new Mensagens();
        return mens;
    } 
    public Mensagens editaProfissional(Profissional prof){
        Mensagens mens = new Mensagens();
        return mens;
    }
    public Profissional buscaProfissional(int id_paciente){
        Profissional prof = new Profissional();
        
        return prof;
    }
}
