package profissional.modelo;

import javax.servlet.http.HttpServletRequest;
import receita.modelo.*;


/**
 *
 * @author Diogo
 */
public class Profissional {

    private int idProfissional;
    private String nome;
    private String funcao;
    private String especialidade;
    private String conselhoProfissional;
    private String estado;
    private String cidade;
    private String login;
    private String senha;
    private String dataCadastro;
    
    public Profissional() {
        this.idProfissional = 0;
        this.nome = null;
        this.funcao = null;
        this.especialidade = null;
        this.conselhoProfissional = null;
        this.estado = null;
        this.cidade = null;
        this.login = null;
        this.senha = null;
        this.dataCadastro = null;
    }
    public Profissional(HttpServletRequest request) {
        this.idProfissional = Integer.parseInt(request.getParameter("id"));
        this.nome = request.getParameter("nome");
        this.funcao = request.getParameter("funcao");
        this.especialidade = request.getParameter("especialidade");
        this.conselhoProfissional = request.getParameter("conselho_profissional");
        this.estado = request.getParameter("estado");
        this.cidade = request.getParameter("cidade");
        this.login = request.getParameter("login");
        this.senha = request.getParameter("senha");
    }

    public int getIdProfissional() {
        return idProfissional;
    }

    public void setIdProfissional(int idProfissional) {
        this.idProfissional = idProfissional;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public String getConselhoProfissional() {
        return conselhoProfissional;
    }

    public void setConselhoProfissional(String conselhoProfissional) {
        this.conselhoProfissional = conselhoProfissional;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(String dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
    
    
    
    
    

}
