package profissional.servlet;

import aplicacao.modelo.Mensagens;
import aplicacao.modelo.Menu;
import conexao.FabricaConexao;
import java.sql.SQLException;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import profissional.dao.ProfissionalDAO;
import profissional.dao.ValidaProfissional;
import profissional.modelo.Profissional;

/**
 *
 * @author Diogo
 */
public class MontarProfissional extends Action {

    HttpSession session;

    @Override
    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws SQLException, ParseException {
        session = request.getSession(true);
        //if (session.getAttribute("login") == null) {
        //    return map.findForward("login");
        //} else {
        Menu m = new Menu();
        Mensagens mens = null;
        request.setAttribute("menu", m);
        if (request.getParameter("pagina") != null) {
            if (ValidaProfissional.validaParametros(m, request.getParameter("pagina"))) {

                //editar Profissional
                if (request.getParameter("pagina").contains(m.getCAD_EDT_PROFISSIONAL())) {
                    //BUSCAR INFORMACOES DO Profissional

                    ProfissionalDAO dao = new ProfissionalDAO();
                    Profissional prof = dao.buscaProfissional(Integer.parseInt(request.getParameter("id")));
                    if (Integer.parseInt(session.getAttribute("id_usuario").toString()) != prof.getIdProfissional()) {
                        prof.setSenha("block");
                    }

                    request.setAttribute("dados", prof);

                    request.setAttribute("opcao", "editar");
                    return map.findForward("fwdCadAddProfissional");
                }
                //cadastrar novo Profissional
                if (request.getParameter("pagina").contains(m.getCAD_ADD_PROFISSIONAL())) {
                    if (!request.getParameter("opcao").contains("")) {
                        mens = ValidaProfissional.validaCampos(request);
                        if (!mens.ok) {//se falso - ERRO
                            //executa crud
                            Profissional prof = new Profissional(request);
                            ProfissionalDAO dao = new ProfissionalDAO(FabricaConexao.getConnection());
                            if (request.getParameter("opcao").contains("adicionar")) {
                                mens = dao.adicionaProfissional(prof);

                                request.setAttribute("opcao", mens.ok ? "editar" : "adicionar");
                            } else if (request.getParameter("opcao").contains("editar")) {
                                mens = dao.editaProfissional(prof);//editar
                            }
                            request.setAttribute("dados", prof);
                        }
                    }
                    request.setAttribute("msg", mens);
                    return map.findForward("fwdCadAddProfissional");
                }
            }
        }
        return map.findForward("fwdCadProfissional");
        //}
    }
}
