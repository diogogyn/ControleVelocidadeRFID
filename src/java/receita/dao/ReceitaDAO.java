/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package receita.dao;

import aplicacao.dao.Funcoes;
import aplicacao.modelo.Mensagens;
import conexao.FabricaConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import paciente.modelo.PacDadosPessoais;
import receita.modelo.ItensReceita;
import receita.modelo.ReceitasPrescritas;

/**
 *
 * @author Diogo
 */
public class ReceitaDAO {

    private Connection con = null;
    private ArrayList<String> erros = null;

    public ReceitaDAO() throws SQLException {
        this.con = FabricaConexao.getConnection();
        erros = new ArrayList<>();
    }

    public ReceitaDAO(Connection con) {
        this.con = con;
        erros = new ArrayList<>();
    }

    /**
     * Destrutor da classe. Ele registra um log com os possiveis erros que
     * ocoreram no sistema.
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            Funcoes.registraLogErros(erros);
            // super.finalize();
        } finally {
            super.finalize();
        }

    }

    /**
     * Busca todos os pacientes cadastrados que satisfaçam uma das condições
     * impostas.
     *
     * @param nome
     * @param numProntuario
     * @param cpf
     * @return
     * @throws SQLException
     */
    public ArrayList<PacDadosPessoais> buscarPacientes(String nome, String numProntuario, String cpf) throws SQLException {
        ArrayList<PacDadosPessoais> lista = null;
        String sql = "SELECT id_paciente, nome_paciente, dt_nascimento, nome_mae FROM pacientes WHERE nome_paciente like %?% OR num_prontuario LIKE %?% OR cpf LIKE %?% LIMIT 15";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, nome);
            stmt.setString(2, numProntuario);
            stmt.setString(3, cpf);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                lista = new ArrayList<>();
                ArrayList<String> columns = new ArrayList<>();
                columns = Funcoes.retornaColunas(rsmd, columns);
                while (rs.next()) {
                    try {
                        lista.add(new PacDadosPessoais(rs, columns));
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("#ERRO METODO(buscarPacientes): "
                                + e.getMessage());
                        erros.add("#ERRO METODO(buscarPacientes): " + e.getMessage());
                    }
                }
            }

        } catch (SQLException e) {
            erros.add("#ERRO METODO(buscarPacientes): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return lista;
    }

    /**
     * Busca todas as receitas cadastradas referente a um paciente.
     *
     * @param idPaciente
     * @return lista
     * @throws SQLException
     */
    public ArrayList<ReceitasPrescritas> buscarReceitas(int idPaciente) throws SQLException {
        ArrayList<ReceitasPrescritas> lista = null;

        String sql = "SELECT id_receita, id_paciente, num_receita, prescritor, status, dt_emissao, dt_finalizacao FROM receitas_prescritas WHERE id_paciente=? LIMIT 20";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, idPaciente);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                lista = new ArrayList<>();
                ArrayList<String> columns = new ArrayList<>();
                columns = Funcoes.retornaColunas(rsmd, columns);
                while (rs.next()) {
                    try {
                        lista.add(new ReceitasPrescritas(rs, columns));
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("#ERRO METODO(buscarReceitas): "
                                + e.getMessage());
                        erros.add("#ERRO METODO(buscarReceitas): " + e.getMessage());
                    }
                }
            }

        } catch (SQLException e) {
            erros.add("#ERRO METODO(buscarReceitas): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return lista;
    }

    /**
     * Busca todas as Solicitações de receitas.
     *
     * @param numReceita
     * @param dtSolicitacoes
     * @param prescritor
     * @param finalizadas
     * @param nomePaciente
     * @return lista
     * @throws SQLException
     */
    public ArrayList<ReceitasPrescritas> buscarSolicitacoesReceitas(String numReceita, String dtSolicitacoes, String prescritor, String nomePaciente, String finalizadas) throws SQLException {
        ArrayList<ReceitasPrescritas> lista = null;

        String sql = "SELECT id_receita, num_receita, prescritor, status, dt_emissao, qtdeItens";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, numReceita);
            stmt.setString(2, dtSolicitacoes);
            stmt.setString(3, prescritor);
            stmt.setString(4, nomePaciente);
            stmt.setString(4, finalizadas);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                lista = new ArrayList<>();
                ArrayList<String> columns = new ArrayList<>();
                columns = Funcoes.retornaColunas(rsmd, columns);
                while (rs.next()) {
                    try {
                        lista.add(new ReceitasPrescritas(rs, columns));
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("#ERRO METODO(buscarSolicitacoesReceitas): "
                                + e.getMessage());
                        erros.add("#ERRO METODO(buscarSolicitacoesReceitas): " + e.getMessage());
                    }
                }
            }

        } catch (SQLException e) {
            erros.add("#ERRO METODO(buscarReceitas): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return lista;
    }

    /**
     * Busca todos os itens cadastrados de uma receita
     *
     * @param idReceita
     * @return lista
     * @throws SQLException
     */
    public ArrayList<ItensReceita> buscarItensReceitas(int idReceita) throws SQLException {
        ArrayList<ItensReceita> lista = null;

        String sql = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, idReceita);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                lista = new ArrayList<>();
                ArrayList<String> columns = new ArrayList<>();
                columns = Funcoes.retornaColunas(rsmd, columns);
                while (rs.next()) {
                    try {
                        lista.add(new ItensReceita(rs, columns));
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("#ERRO METODO(buscarItensReceitas): "
                                + e.getMessage());
                        erros.add("#ERRO METODO(buscarItensReceitas): " + e.getMessage());
                    }
                }
            }

        } catch (SQLException e) {
            erros.add("#ERRO METODO(buscarItensReceitas): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return lista;
    }

    /**
     * Busca todos os medicamentos, listando o local de armazenamento, cadastrados de uma receita
     *
     * @param idReceita
     * @return lista
     * @throws SQLException
     */
    public ArrayList<ItensReceita> buscarItensFinalizarReceitas(int idReceita) throws SQLException {
        ArrayList<ItensReceita> lista = null;

        String sql = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, idReceita);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                lista = new ArrayList<>();
                ArrayList<String> columns = new ArrayList<>();
                columns = Funcoes.retornaColunas(rsmd, columns);
                while (rs.next()) {
                    try {
                        lista.add(new ItensReceita(rs, columns));
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("#ERRO METODO(buscarItensReceitas): "
                                + e.getMessage());
                        erros.add("#ERRO METODO(buscarItensReceitas): " + e.getMessage());
                    }
                }
            }

        } catch (SQLException e) {
            erros.add("#ERRO METODO(buscarItensReceitas): " + e.getErrorCode() + " - " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return lista;
    }

    /**
     * Cadastra uma nova receita, salvando os itens da mesma.
     *
     * @param itens
     * @param rc
     * @return mens
     * @throws SQLException
     */
    public Mensagens addNovaReceita(ArrayList<ItensReceita> itens, ReceitasPrescritas rc) throws SQLException {
        Mensagens mens = new Mensagens(false, null);
        String sqlReceita = "INSERT INTO receitas_prescritas "
                + "(id_paciente, num_receita, id_prescritor, status, dt_emissao, qtde_itens)"
                + "VALUES (?,?,?,?,now(),?)"
                + "returning id_receita";
        String sqlItens = "INSERT INTO itens_receita "
                + "(id_receita, id_prescritor, cod_medicamento, intervalo,"
                + " qtde_prescrita, tempo_tratamento, qtde_total, dt_inclusao)"
                + "?,?,?,?,?,?,?,now()";
        String sqlItensEstoque = "";
        String sqlItensEstoqueUpdate = "";
        PreparedStatement stmtReceita = null;
        PreparedStatement stmtItens = null;
        PreparedStatement stmtItensEstoque = null;
        PreparedStatement stmtItensEstoqueUpdate = null;
        ResultSet rs = null;
        ResultSet rsItensEstoque = null;
        if (itens.size() > 0) {
            try {
                /*ATUALIZAR INFO DA RECEITA*/
                con.setAutoCommit(false);
                //2016-0814-0002-0014-235
                stmtReceita = con.prepareStatement(sqlReceita);
                rc.setNumReceita("ANO - MES+DIA -" + rc.getIdPrescritor() + "-" + rc.getIdPaciente() + "-" + itens.size());
                rc.setStatus("ABERTA");
                stmtReceita.setInt(1, rc.getIdPaciente());
                stmtReceita.setInt(2, rc.getIdReceita());
                stmtReceita.setString(3, rc.getNumReceita());
                stmtReceita.setInt(4, rc.getIdPrescritor());
                stmtReceita.setString(5, rc.getStatus());
                stmtReceita.setInt(6, itens.size());

                //Registra a receita
                rs = stmtReceita.executeQuery();

                /*ADICIONAR e REGISTRAR OS ITENS DA RECEITA*/
                stmtItens = con.prepareStatement(sqlItens);

                for (ItensReceita it : itens) {
                    it.setCalculaQtdeTotal();
                    stmtItens.setInt(1, rs.getInt("id_receita"));
                    stmtItens.setInt(2, rc.getIdPrescritor());
                    stmtItens.setInt(3, it.getCodMedicamento());
                    stmtItens.setInt(4, it.getIntervalo());
                    stmtItens.setInt(5, it.getQtdePrescrita());
                    stmtItens.setInt(6, it.getTempoTratamento());
                    stmtItens.setInt(7, it.getQtdeTotal());//qtde a dispensar
                    stmtItens.execute();

                    /*FAZER A RESERVA DO MEDICAMENTO NO BANCO DE DADOS*/
                    //buscará os itens por ordem de vencimento e local de armazenamento, para fazer 
                    //a reserva do medicamento para a receita.
                    stmtItensEstoque = con.prepareStatement(sqlItensEstoque + " LIMIT " + it.getQtdeTotal());
                    stmtItensEstoque.setInt(1, it.getCodMedicamento());//buscar o codigo do medicamento

                    rsItensEstoque = stmtItensEstoque.executeQuery();

                    rsItensEstoque.last();
                    int totalOfRecords = rs.getRow();
                    rsItensEstoque.beforeFirst();

                    stmtItensEstoqueUpdate = con.prepareStatement(sqlItensEstoqueUpdate);//update em itens estoque
                    if (totalOfRecords >= it.getQtdeTotal()) {//verifica se tem a quantidade necessaria de medicamentos para o pedido
                        while (rsItensEstoque.next()) {
                            stmtItensEstoqueUpdate.setInt(1, rs.getInt("id_receita"));
                            stmtItensEstoqueUpdate.setInt(2, rsItensEstoque.getInt("id_item"));//where - id do item no estoque
                            stmtItensEstoqueUpdate.execute();
                            mens.ok = true;
                        }

                    } else {
                        con.rollback();
                        mens.ok = false;
                        mens.mensagem = "Sinto muito, mas não há mais estoque suficiente para a quantidade desejada para o medicamento COD: " + it.getCodMedicamento() + ". Tente uma quantidade menor.";
                    }

                }
                if (mens.ok) {
                    con.commit();
                    mens.ok = true;
                    mens.mensagem = "Receita salva com sucesso!";
                }

            } catch (SQLException e) {
                con.rollback();
                erros.add("#ERRO METODO(addNovaReceita): " + e.getErrorCode() + " - " + e.getMessage());
                mens.mensagem = "ERRO: Não foi possivel salvar a receita.";
            } catch (IndexOutOfBoundsException e) {
                con.rollback();
                erros.add("#ERRO METODO(addNovaReceita): " + e.getMessage());
                mens.mensagem = "ERRO: Não foi possivel continuar. Ocorreu algum erro de conversão.";
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (rsItensEstoque != null) {
                    rsItensEstoque.close();
                }
                if (stmtReceita != null) {
                    stmtReceita.close();
                }
                if (stmtItens != null) {
                    stmtItens.close();
                }
                if (stmtItensEstoque != null) {
                    stmtItensEstoque.close();
                }
                if (stmtItensEstoqueUpdate != null) {
                    stmtItensEstoqueUpdate.close();
                }

            }
        } else {
            mens.mensagem = "Não existe itens cadastrados para registar uma receita.";
        }
        return mens;
    }

    /**
     * Faz a atualização dos itens de uma receia e atualiza a situação da mesma
     *
     * @param itens
     * @param rc
     * @return mens
     * @throws SQLException
     */
    public Mensagens atualizaReceita(ArrayList<ItensReceita> itens, ReceitasPrescritas rc) throws SQLException {
        Mensagens mens = new Mensagens(false, null);
        String sqlUpReceita = "UPDATE receitas_prescritas SET "
                + "id_prescritor = ?, status = ?, qtde_itens = ?, dt_emissao = now() "
                + "WHERE id_paciente = ? AND id_receita = ?";
        String sqlItensAdd = "INSERT INTO itens_receita "
                + "(id_receita, id_prescritor, cod_medicamento, intervalo,"
                + " qtde_prescrita, tempo_tratamento, qtde_total, dt_inclusao)"
                + "?,?,?,?,?,?,?,now()";

        String sqlItensDel = "DLETE * FROM itens_receita";
        String sqlItensEstoque = "";
        String sqlItensEstoqueUpdate = "";
        String sqlItensEstoqueDel = "";
        PreparedStatement stmtUpReceita = null;
        PreparedStatement stmtItensAdd = null;
        PreparedStatement stmtItensDel = null;
        PreparedStatement stmtItensEstoque = null;
        PreparedStatement stmtItensEstoqueUpdate = null;
        PreparedStatement stmtItensEstoqueDel = null;
        ResultSet rsItensEstoque = null;
        ResultSet rs = null;
        int qtde;
        if (itens.size() > 0) {
            try {
                /*BUSCAR OS ITENS DA RECEITA CADASTRADA*/
                ArrayList<ItensReceita> itensCadastrados = this.buscarItensReceitas(rc.getIdReceita());

                /*separa quais os itens que o usuario removeu da lista de medicamentos para gerar o where*/
                ArrayList<ItensReceita> itensDel = this.verificaItensParaDeletar(itensCadastrados, itens);//se econtrar, ele tira da lista
                String idsItensDel;

                /*Procura quais foram os novos itens adicionados pelo usuario*/
                itens = this.deletaItensReceita(itens, itensCadastrados);//itens que serão adicionados
                /*RECALCULA A QTDE TOTAL DE ITENS DA RECEITA*/
                qtde = (itensCadastrados.size() - itensDel.size()) + itens.size();
                if (itensDel.size() > 0 || itens.size() > 0) {
                    con.setAutoCommit(false);

                    /*ATUALIZAR INFO DA RECEITA*/
                    stmtUpReceita = con.prepareStatement(sqlUpReceita);
                    rc.setStatus("ABERTA");//MODIFICA A SITUACAO DA RECEITA

                    stmtUpReceita.setInt(1, rc.getIdPrescritor());
                    stmtUpReceita.setString(2, rc.getStatus());
                    stmtUpReceita.setInt(3, qtde);
                    stmtUpReceita.setInt(4, rc.getIdPaciente());
                    stmtUpReceita.setInt(5, rc.getIdReceita());

                    stmtUpReceita.execute();

                    /*ADICIONAR OS NOVOS ITENS DA RECEITA*/
                    if (itens.size() > 0) {
                        stmtItensAdd = con.prepareStatement(sqlItensAdd);

                        for (ItensReceita it : itens) {
                            it.setCalculaQtdeTotal();
                            stmtItensAdd.setInt(2, rc.getIdReceita());
                            stmtItensAdd.setInt(3, rc.getIdPrescritor());
                            stmtItensAdd.setInt(4, it.getCodMedicamento());
                            stmtItensAdd.setInt(5, it.getIntervalo());
                            stmtItensAdd.setInt(6, it.getQtdePrescrita());
                            stmtItensAdd.setInt(7, it.getTempoTratamento());
                            stmtItensAdd.setInt(8, it.getQtdeTotal());//qtde a dispensar

                            stmtItensAdd.execute();
                            /*FAZER A RESERVA DO MEDICAMENTO NO BANCO DE DADOS*/
                            //buscará os itens por ordem de vencimento e local de armazenamento, para fazer 
                            //a reserva do medicamento para a receita.
                            stmtItensEstoque = con.prepareStatement(sqlItensEstoque + " LIMIT " + it.getQtdeTotal());
                            stmtItensEstoque.setInt(1, it.getCodMedicamento());//buscar o codigo do medicamento

                            rsItensEstoque = stmtItensEstoque.executeQuery();

                            rsItensEstoque.last();
                            int totalOfRecords = rsItensEstoque.getRow();//QUANTIDADE DE REGISTROS ENCONTRADOS
                            rsItensEstoque.beforeFirst();

                            stmtItensEstoqueUpdate = con.prepareStatement(sqlItensEstoqueUpdate);//update em itens estoque
                            if (totalOfRecords >= it.getQtdeTotal()) {//verifica se tem a quantidade necessaria de medicamentos para o pedido
                                while (rsItensEstoque.next()) {
                                    stmtItensEstoqueUpdate.setInt(1, rc.getIdReceita());
                                    stmtItensEstoqueUpdate.setInt(2, rsItensEstoque.getInt("id_item"));//where - id do item no estoque
                                    stmtItensEstoqueUpdate.execute();
                                    mens.ok = true;
                                }

                            } else {
                                con.rollback();
                                mens.ok = false;
                                mens.mensagem = "Sinto muito, mas não há mais estoque suficiente para a quantidade desejada para o medicamento COD: " + it.getCodMedicamento() + ". Tente uma quantidade menor.";
                            }
                        }

                    }
                    if (mens.ok) {//para verificar se conseguiu realizar os passos anteriores
                        if (itensDel.size() > 0) {
                            idsItensDel = " WHERE id_item " + this.retornaIdItensReceita(itensDel);
                            /*REMOVER OS ITENS QUE FORAM REMOVIDOS DA RECEITA PELO USUARIO*/
                            stmtItensDel = con.prepareStatement(sqlItensDel + idsItensDel);
                            stmtItensDel.execute();//deleta os itens da tabela itens_receita

                            stmtItensEstoqueDel = con.prepareStatement(sqlItensEstoqueDel);//update em itens estoque
                            for (ItensReceita id : itensDel) {
                                stmtItensEstoqueDel.setInt(1, 0);//zera o id da receita em itens_estoque //procurar futuramente como setar null neste campo sem prejudicar o sql
                                stmtItensEstoqueDel.setInt(2, rc.getIdReceita());//where - id da receita
                                stmtItensEstoqueDel.setInt(3, id.getCodMedicamento());//where - cod_medicamento
                                stmtItensEstoqueDel.execute();
                            }
                        }
                        con.commit();
                        mens.ok = true;
                        mens.mensagem = "Receita atualizada com sucesso!";
                    }
                } else {
                    mens.mensagem = "Não houve alterações na lista de itens para que se atualize a receita.";
                }
            } catch (SQLException e) {
                con.rollback();
                erros.add("#ERRO METODO(atualizaReceita): " + e.getErrorCode() + " - " + e.getMessage());
                mens.mensagem = "ERRO: Não foi possivel atualizar a receita.";
            } catch (IndexOutOfBoundsException e) {
                con.rollback();
                erros.add("#ERRO METODO(atualizaReceita): " + e.getMessage());
                mens.mensagem = "ERRO: Não foi possivel continuar. Ocorreu algum erro de conversão.";
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (rsItensEstoque != null) {
                    rsItensEstoque.close();
                }
                if (stmtItensAdd != null) {
                    stmtItensAdd.close();
                }
                if (stmtItensDel != null) {
                    stmtItensDel.close();
                }
                if (stmtUpReceita != null) {
                    stmtUpReceita.close();
                }
                if (stmtItensEstoque != null) {
                    stmtItensEstoque.close();
                }
                if (stmtItensEstoqueUpdate != null) {
                    stmtItensEstoqueUpdate.close();
                }
                if (stmtItensEstoqueDel != null) {
                    stmtItensEstoqueDel.close();
                }

            }
        } else {

        }
        return mens;
    }

    /**
     * Finaliza uma receita.
     *
     * @param idReceita
     * @return mens
     * @throws SQLException
     */
    public Mensagens finalizarReceita(int idReceita) throws SQLException {
        Mensagens mens = new Mensagens(false, null);
        String sql = null;
        PreparedStatement stmt = null;
        try {
            /*ATUALIZAR INFO DA RECEITA*/
            con.setAutoCommit(false);
            stmt = con.prepareStatement(sql);

            stmt.setInt(1, idReceita);
            stmt.execute();
            con.commit();
            mens.ok = true;
            mens.mensagem = "Receita finalizada com sucesso!";

        } catch (SQLException e) {
            con.rollback();
            erros.add("#ERRO METODO(finalizarReceita): " + e.getErrorCode() + " - " + e.getMessage());
            mens.mensagem = "ERRO: Não foi possivel finalizar a receita.";
        } catch (IndexOutOfBoundsException e) {
            con.rollback();
            erros.add("#ERRO METODO(finalizarReceita): " + e.getMessage());
            mens.mensagem = "ERRO: Não foi possivel continuar. Ocorreu algum erro de conversão.";
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return mens;
    }

    /**
     * Permite adicionar um item a lista de medicamentos de uma receita,
     * retornando a lista em seguida
     *
     * @param itens
     * @param novoItem
     * @return itens
     */
    public ArrayList<ItensReceita> adicionaItensReceita(ArrayList<ItensReceita> itens, ItensReceita novoItem) {
        itens.add(novoItem);
        return itens;
    }

    public Mensagens verificaEstoque(ItensReceita novoItem) {
        Mensagens mens = new Mensagens(false, null);
        String sqlItennEstoque = "SELECT SUM(id_item) as qtde FROM itens_estoque WHERE cod_medicamento=?";
        PreparedStatement stmtItensEstoque;
        ResultSet rs;
        try {
            con.setAutoCommit(false);

            /*ATUALIZAR INFO DA RECEITA*/
            stmtItensEstoque = con.prepareStatement(sqlItennEstoque);
            stmtItensEstoque.setInt(1, novoItem.getCodMedicamento());
            rs = stmtItensEstoque.executeQuery();
            if (rs.next()) {
                if (novoItem.getQtdeTotal() <= rs.getInt("qtde")) {
                    mens.ok = true;
                } else {
                    mens.mensagem = "Não há estoque de medicamentos no estoque para a quantidade desejada.";
                }
            } else {
                mens.mensagem = "Este medicamento não esta registrado no estoque";
            }
            rs.close();
            stmtItensEstoque.close();

        } catch (SQLException e) {
            erros.add("#ERRO METODO(verificaEstoque): " + e.getErrorCode() + " - " + e.getMessage());
            mens.mensagem = "ERRO: Não foi possivel realizar a consulta.";
        } catch (IndexOutOfBoundsException e) {
            erros.add("#ERRO METODO(verificaEstoque): " + e.getMessage());
            mens.mensagem = "ERRO: Não foi possivel continuar. Ocorreu algum erro de conversão.";
        }
        return mens;
    }

    /**
     * Permite excluir um item da lista de medicamentos
     *
     * @param itens
     * @param idItem
     * @return itens
     */
    public ArrayList<ItensReceita> deletaItensReceita(ArrayList<ItensReceita> itens, int idItem) {
        for (ItensReceita ic : itens) {
            if (idItem == ic.getIdItem()) {
                itens.remove(ic);
                break;
            }
        }
        return itens;
    }

    /**
     * Permite excluir varios itens da lista de medicamentos de uma unica vez
     * por Intercalação. Ele retira todos os elementos que estão em itensLista e
     * que não estão em itensRetirar
     *
     * @param itensLista
     * @param itensRetirar
     * @return itens
     */
    public ArrayList<ItensReceita> deletaItensReceita(ArrayList<ItensReceita> itensRetirar, ArrayList<ItensReceita> itensLista) {
        int lIdItensLista;
        for (int i = 0; i < itensLista.size(); i++) {
            lIdItensLista = itensLista.get(i).getIdItem();
            for (ItensReceita ic : itensRetirar) {
                if (lIdItensLista == ic.getIdItem()) {
                    itensRetirar.remove(ic);
                    break;
                }
            }
        }

        return itensRetirar;
    }

    /**
     * Gera a condição(WHERE) dos itens que podem ser usados em uma operação SQL
     * receita.
     *
     * @param itensRetirar
     * @param itensLista
     * @return listaIdsItens
     */
    private String retornaIdItensReceita(ArrayList<ItensReceita> itensLista) {
        String listaIdsItens = null;

        if (itensLista.size() > 0) {
            for (int i = 0; i < itensLista.size() - 1; i++) {
                listaIdsItens = listaIdsItens + itensLista.get(i).getIdItem() + ", ";
            }
            listaIdsItens = " in (" + listaIdsItens.substring(0, (listaIdsItens.length() - 2)) + ") ";
        }
        return listaIdsItens;
    }

    /**
     * Verifica quais itens da lista de medicamentos, foram removidos pelo
     * usuario
     *
     * @param itensRetirar
     * @param itensLista
     * @return
     */
    private ArrayList<ItensReceita> verificaItensParaDeletar(ArrayList<ItensReceita> itensRetirar, ArrayList<ItensReceita> itensLista) {
        int lIdItensLista;
        for (int i = 0; i < itensLista.size(); i++) {
            lIdItensLista = itensLista.get(i).getIdItem();
            for (ItensReceita ic : itensRetirar) {
                if (lIdItensLista == ic.getIdItem()) {
                    itensRetirar.remove(ic);
                    break;
                }
            }
        }
        return itensRetirar;
    }

    /**
     * metodo não utilizado.
     *
     * @param itens
     * @return rc
     */
    private ReceitasPrescritas calculaItensReceita(ArrayList<ItensReceita> itens) {
        ReceitasPrescritas rc = new ReceitasPrescritas();
        itens.stream().forEach((ic) -> {
            rc.setQtdeTotal(rc.getQtdeTotal() + ic.getQtdeTotal());
        });
        rc.setNumReceita(itens.get(0).getNumReceita());
        rc.setIdPaciente(itens.get(0).getIdPaciente());
        return rc;
    }
}
