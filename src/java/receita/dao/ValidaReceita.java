package receita.dao;

import profissional.dao.*;
import paciente.dao.*;
import aplicacao.dao.Funcoes;
import aplicacao.modelo.Mensagens;
import aplicacao.modelo.Menu;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rfid
 */
public class ValidaReceita {

    public static boolean validaParametros(Menu m, String param) {
        if (param != null) {
            if (param.equals(m.getREC_NOVA())) {
                return true;
            } else if (param.equals(m.getREC_COMPLEMENTAR())) {
                return true;
            }else {
                return param.equals(m.getREC_FINALIZAR());
            }
        } else {
            return false;
        }
    }

    public static Mensagens validaCampos(HttpServletRequest request) {
        if (request.getParameter("nome") == null || request.getParameter("nome") == "") {
            return new Mensagens(false, "Você deixou o campo nome em branco.");
        } else if (request.getParameter("funcao") == null || request.getParameter("funcao") == "") {
            return new Mensagens(false, "Você deixou o campo função em branco.");
        } else if (request.getParameter("especialidade") == null || request.getParameter("especialidade") == "") {
            return new Mensagens(false, "Você não informou uma especialidade (Cargo, atribuição, etc).");
        } else if (request.getParameter("estado") == null || request.getParameter("estado") == "") {
            return new Mensagens(false, "Você informou um estado inválido.");
        } else if (request.getParameter("cidade") == null || request.getParameter("cidade") == "") {
            return new Mensagens(false, "Você deixou o campo cidade em branco.");
        } else if (request.getParameter("login") == null || request.getParameter("login") == "") {
            return new Mensagens(false, "Voce deixou o campo login em branco.");
        } else if (Funcoes.validaCPF(request.getParameter("cpf"))) {
            return new Mensagens(false, "O CPF informado é invalido.");
        } else if (request.getParameter("senha") == null || request.getParameter("senha") == "") {
            return new Mensagens(false, "Você não informou uma senha.");
        } else if (request.getParameter("senha2") == null || request.getParameter("senha2") == "") {
            return new Mensagens(false, "Você não confirmou a senha.");
        } else if (request.getParameter("senha") != request.getParameter("senha2")) {
            return new Mensagens(false, "As senhas não conferem.");
        } else {
            return new Mensagens(true, null);
        }
    }
    
    public static Mensagens validaCamposAddItem(HttpServletRequest request) {
        if (request.getParameter("medicamento") == null || request.getParameter("medicamento") == "") {
            return new Mensagens(false, "Você não informou um medicamento.");
        } else if (request.getParameter("qtde_prescrita") == null || request.getParameter("qtde_prescrita") == "") {
            return new Mensagens(false, "Você deve informar uma qtde a ser prescrita.");
        } else if (request.getParameter("intervalo") == null || request.getParameter("intervalo") == "") {
            return new Mensagens(false, "Você não informou um intervalo para medicação.");
        } else if (request.getParameter("tempo_tratamento") == null || request.getParameter("tempo_tratamento") == "") {
            return new Mensagens(false, "Você deve informar o tempo de tratamento para calculo de quantidade a ser retirada.");
        } else {
            return new Mensagens(true, null);
        }
    }
}
