package receita.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Diogo
 */
public class ItensReceita extends Receita {

    private int idItem;
    private int codMedicamento;
    private int intervalo;
    private int tempoTratamento;
    private int qtdePrescrita;
    private String descricaoPrescricao;

    public ItensReceita() {
        this.idItem = 0;
        this.codMedicamento = 0;
        this.intervalo = 0;
        this.tempoTratamento = 0;
        this.qtdePrescrita = 0;
        this.descricaoPrescricao = null;
    }

    //s ópara testes - exluir depois
    public ItensReceita(int idItem) {
        this.idItem = idItem;
        this.codMedicamento = 0;
        this.intervalo = 0;
        this.tempoTratamento = 0;
        this.qtdePrescrita = 0;
        this.descricaoPrescricao = null;
        area = 0;
        rua = 0;
        modulo = 0;
        nivel = 0;
        vao = 0;
    }

    public ItensReceita(HttpServletRequest request) {

        this.idItem = request.getParameter("id_item") != null
                ? Integer.parseInt(request.getParameter("id_item"))
                : 0;
        this.codMedicamento = request.getParameter("codigo_medicamento") != null
                ? Integer.parseInt(request.getParameter("codigo_medicamento")) : 0;
        this.intervalo = request.getParameter("intervalo") != null
                ? Integer.parseInt(request.getParameter("intervalo")) : 0;
        this.tempoTratamento = request.getParameter("tempo_tratamento") != null
                ? Integer.parseInt(request.getParameter("tempo_tratamento")) : 0;
        this.qtdePrescrita = request.getParameter("qtde_prescrita") != null
                ? Integer.parseInt(request.getParameter("qtde_prescrita")) : 0;
        this.descricaoPrescricao = null;

        qtdeTotal = (this.qtdePrescrita * this.tempoTratamento) / this.intervalo;
        this.status = request.getParameter("status") != null
                ? request.getParameter("status") : null;
        area = request.getParameter("area") != null
                ? Integer.parseInt(request.getParameter("area")) : 0;
        rua = request.getParameter("rua") != null
                ? Integer.parseInt(request.getParameter("modulo")) : 0;
        nivel = request.getParameter("nivel") != null
                ? Integer.parseInt(request.getParameter("nivel")) : 0;
        vao = request.getParameter("vao") != null
                ? Integer.parseInt(request.getParameter("vao")) : 0;
    }

    public ItensReceita(ResultSet rs, ArrayList<String> columns) throws SQLException {
        this.idItem = columns.contains("id_item") ? rs.getInt("id_item") : 0;
        this.codMedicamento = columns.contains("codigo_medicamento") ? rs.getInt("codigo_medicamento") : 0;
        this.intervalo = columns.contains("area") ? rs.getInt("area") : 0;
        this.tempoTratamento = columns.contains("tempo_tratamento") ? rs.getInt("tempo_tratamento") : 0;
        this.qtdePrescrita = columns.contains("qtde_prescrita") ? rs.getInt("qtde_prescrita") : 0;
        this.descricaoPrescricao = columns.contains("descricao_prescricao") ? rs.getString("descricao_prescricao") : null;
        status = columns.contains("status") ? rs.getString("status") : null;
        area = columns.contains("area") ? rs.getInt("area") : 0;
        rua = columns.contains("rua") ? rs.getInt("rua") : 0;
        modulo = columns.contains("modulo") ? rs.getInt("modulo") : 0;
        nivel = columns.contains("nivel") ? rs.getInt("nivel") : 0;
        vao = columns.contains("vao") ? rs.getInt("vao") : 0;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public int getCodMedicamento() {
        return codMedicamento;
    }

    public void setCodMedicamento(int codMedicamento) {
        this.codMedicamento = codMedicamento;
    }

    public int getIntervalo() {
        return intervalo;
    }

    public void setIntervalo(int intervalo) {
        this.intervalo = intervalo;
    }

    public int getTempoTratamento() {
        return tempoTratamento;
    }

    public void setTempoTratamento(int tempoTratamento) {
        this.tempoTratamento = tempoTratamento;
    }

    public String getDescricaoPrescricao() {
        return descricaoPrescricao;
    }

    public void setDescricaoPrescricao(String descricaoPrescricao) {
        this.descricaoPrescricao = descricaoPrescricao;
    }

    public int getQtdePrescrita() {
        return qtdePrescrita;
    }

    public void setQtdePrescrita(int qtdePrescrita) {
        this.qtdePrescrita = qtdePrescrita;
    }

    public void setCalculaQtdeTotal() {
        qtdeTotal = (this.qtdePrescrita * this.tempoTratamento) / this.intervalo;
    }
}
