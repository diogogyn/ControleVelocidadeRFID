package receita.modelo;

/**
 *
 * @author Diogo
 */
public class Receita {

    protected int idReceita;
    protected int idPaciente;
    private int idMedicamento;
    protected String nomeMedicamento;
    protected String unidSaida;
    protected String numReceita;
    protected int idPrescritor;
    protected String prescritor;
    protected int qtdeTotal;
    protected String status;

    /*local de armazenamento*/
    protected int area;
    protected int rua;
    protected int modulo;
    protected int nivel;
    protected int vao;

    public Receita() {
        this.idReceita = 0;
        this.idPaciente = 0;
        this.idMedicamento = 0;
        this.nomeMedicamento = null;
        this.unidSaida = null;
        this.numReceita = null;
        this.prescritor = null;
        this.idPrescritor = 0;
        this.qtdeTotal = 0;
        this.status = null;
        this.area = 0;
        this.rua = 0;
        this.modulo = 0;
        this.nivel = 0;
        this.vao = 0;

    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getRua() {
        return rua;
    }

    public void setRua(int rua) {
        this.rua = rua;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getVao() {
        return vao;
    }

    public void setVao(int vao) {
        this.vao = vao;
    }

    public int getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public int getIdReceita() {
        return idReceita;
    }

    public void setIdReceita(int idReceita) {
        this.idReceita = idReceita;
    }

    public int getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(int idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getNomeMedicamento() {
        return nomeMedicamento;
    }

    public void setNomeMedicamento(String nomeMedicamento) {
        this.nomeMedicamento = nomeMedicamento;
    }

    public String getUnidSaida() {
        return unidSaida;
    }

    public void setUnidSaida(String unidSaida) {
        this.unidSaida = unidSaida;
    }

    public String getNumReceita() {
        return numReceita;
    }

    public void setNumReceita(String numReceita) {
        this.numReceita = numReceita;
    }

    public String getPrescritor() {
        return prescritor;
    }

    public void setPrescritor(String prescritor) {
        this.prescritor = prescritor;
    }

    public int getIdPrescritor() {
        return idPrescritor;
    }

    public void setIdPrescritor(int idPrescritor) {
        this.idPrescritor = idPrescritor;
    }

    public int getQtdeTotal() {
        return qtdeTotal;
    }

    public void setQtdeTotal(int qtdeTotal) {
        this.qtdeTotal = qtdeTotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
