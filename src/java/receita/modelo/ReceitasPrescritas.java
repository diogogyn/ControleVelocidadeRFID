package receita.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Diogo
 */
public class ReceitasPrescritas extends Receita {

    protected String dtValidade;
    

    private int codMedicamento;
    private String dtEmissao;
    private String dtFinalizacao;

    public ReceitasPrescritas() {
        area = 0;
        rua = 0;
        modulo = 0;
        nivel = 0;
        vao = 0;
        this.codMedicamento = 0;
        this.dtValidade = null;
        this.dtEmissao = null;
        this.dtFinalizacao = null;
        
        idReceita = 0;
        nomeMedicamento = null;
        numReceita = null;
        prescritor = null;
        qtdeTotal = 0;
        status = null;
    }

    public ReceitasPrescritas(int area, int rua, int modulo, int nivel, int vao, int codMedicamento, String dtValidade, String dtEmissao, String dtFinalizacao, String status) {
        this.area = area;
        this.rua = rua;
        this.modulo = modulo;
        this.nivel = nivel;
        this.vao = vao;
        this.codMedicamento = codMedicamento;
        this.dtValidade = dtValidade;
        this.dtEmissao = dtEmissao;
        this.dtFinalizacao = dtFinalizacao;
        this.status = status;
    }

    public ReceitasPrescritas(ResultSet rs, ArrayList<String> columns) throws SQLException {
        this.area = columns.contains("area") ? rs.getInt("area") : 0;
        this.rua = columns.contains("rua") ? rs.getInt("rua") : 0;
        this.modulo = columns.contains("modulo") ? rs.getInt("modulo") : 0;
        this.nivel = columns.contains("nivel") ? rs.getInt("nivel") : 0;
        this.vao = columns.contains("vao") ? rs.getInt("vao") : 0;
        this.codMedicamento = columns.contains("cod_medicamento") ? rs.getInt("cod_medicamento") : 0;
        this.dtValidade = columns.contains("dt_validade") ? rs.getString("dt_validade") : null;
        this.dtEmissao = columns.contains("dt_emissao") ? rs.getString("dt_emissao") : null;
        this.dtFinalizacao = columns.contains("dt_finalizacao") ? rs.getString("dt_finalizacao") : null;
        this.status = columns.contains("status") ? rs.getString("status") : null;
        idReceita = columns.contains("id_receita") ? rs.getInt("id_receita") : 0;
        nomeMedicamento = columns.contains("nome_medicamento") ? rs.getString("nome_medicamento") : null;
        numReceita = columns.contains("num_receita") ? rs.getString("num_receita") : null;
        prescritor = columns.contains("prescritor") ? rs.getString("prescritor") : null;
        qtdeTotal = columns.contains("qtde_total") ? rs.getInt("qtde_total") : 0;
        status = columns.contains("status") ? rs.getString("status") : null;

    }

    public ReceitasPrescritas(HttpServletRequest request) {
        this.area = request.getParameter("area") != null
                ? Integer.parseInt(request.getParameter("area")) : 0;
        this.rua = request.getParameter("rua") != null
                ? Integer.parseInt(request.getParameter("modulo")) : 0;
        this.nivel = request.getParameter("nivel") != null
                ? Integer.parseInt(request.getParameter("nivel")) : 0;
        this.vao = request.getParameter("vao") != null
                ? Integer.parseInt(request.getParameter("vao")) : 0;
        this.codMedicamento = request.getParameter("cod_medicamento") != null
                ? Integer.parseInt(request.getParameter("cod_medicamento")) : 0;
        this.dtValidade = request.getParameter("dt_validade") != null
                ? request.getParameter("dt_validade") : null;
        this.dtEmissao = request.getParameter("dt_emissao") != null
                ? request.getParameter("dt_emissao") : null;
        this.dtFinalizacao = request.getParameter("dt_finalizacao") != null
                ? request.getParameter("dt_finalizacao") : null;
        this.status = request.getParameter("status") != null
                ? request.getParameter("status") : null;
        idReceita = request.getParameter("id_receita") != null
                ? Integer.parseInt(request.getParameter("id_receita")) : 0;
        nomeMedicamento = request.getParameter("nome_medicamento") != null ? request.getParameter("nome_medicamento") : null;
        numReceita = request.getParameter("num_receita") != null
                ? request.getParameter("num_receita") : "2016-0912-1141";//ANO- MES+DIA - HORA+MINUTO
        prescritor = request.getParameter("prescritor") != null
                ? request.getParameter("prescritor") : null;
        qtdeTotal = request.getParameter("qtde_total") != null
                ? Integer.parseInt(request.getParameter("qtde_total")) : 0;
        status = request.getParameter("status") != null
                ? request.getParameter("status") : null;
        
    }

    public int getCodMedicamento() {
        return codMedicamento;
    }

    public void setCodMedicamento(int codMedicamento) {
        this.codMedicamento = codMedicamento;
    }

    public String getDtValidade() {
        return dtValidade;
    }

    public void setDtValidade(String dtValidade) {
        this.dtValidade = dtValidade;
    }

    public String getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(String dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public String getDtFinalizacao() {
        return dtFinalizacao;
    }

    public void setDtFinalizacao(String dtFinalizacao) {
        this.dtFinalizacao = dtFinalizacao;
    }

}
