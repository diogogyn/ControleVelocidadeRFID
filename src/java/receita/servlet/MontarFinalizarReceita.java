package receita.servlet;

import aplicacao.modelo.Mensagens;
import aplicacao.modelo.Menu;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import profissional.dao.ValidaProfissional;
import receita.dao.ReceitaDAO;
import receita.modelo.ItensReceita;
import receita.modelo.ReceitasPrescritas;

/**
 *
 * @author Diogo
 */
public class MontarFinalizarReceita extends Action {

    HttpSession session;

    @Override
    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ParseException {
        session = request.getSession(true);
        //if (session.getAttribute("login") == null) {
        //    return map.findForward("login");
        //} else {
        Mensagens mens = null;
        Menu m = new Menu();
        @SuppressWarnings("UnusedAssignment")
        String btnFinalizarReceita = "#";//para tivar o botao de incluir itens e Salvar receita
        request.setAttribute("menu", m);
        ArrayList<ReceitasPrescritas> listaReceitas = (ArrayList<ReceitasPrescritas>)request.getAttribute("listaReceitas");
        request.setAttribute("listaReceitas", listaReceitas);

        if (request.getParameter("pagina") != null) {
            if (ValidaProfissional.validaParametros(m, request.getParameter("pagina"))) {
                ReceitaDAO dao = new ReceitaDAO();

                /*BUSCAR PACIENTES CADASTRADOS*/
                if (request.getParameter("pagina").contains(m.getREC_BUSCAR_RECEITAS())) {
                    /*BUSCAR RECEITAS*/
                    listaReceitas = dao.buscarSolicitacoesReceitas(
                            request.getParameter("num_receita"),
                            request.getParameter("dt_solicitacao"),
                            request.getParameter("prescritor"),
                            request.getParameter("nome_paciente"),
                            request.getParameter("finalizadas"));
                    if (listaReceitas.size() > 0) {
                        request.setAttribute("listaReceitas", listaReceitas);
                        btnFinalizarReceita = m.getREC_FINALIZAR();
                    }

                } else if (request.getParameter("pagina").contains(m.getREC_BUSCAR_ITENS_RECEITA())) {
                    /*BUSCAR ITENS DA RECEITA*/
                    request.setAttribute("id_receita", request.getParameter("id_receita"));
                    //TROCAR ESTE METODO
                    request.setAttribute("listaItens",
                            dao.buscarItensFinalizarReceitas(Integer.parseInt(request.getParameter("id_receita"))));
                    btnFinalizarReceita = m.getREC_COMPLEMENTAR();

                } else if (request.getParameter("pagina").contains(m.getREC_FINALIZAR())) {
                    /*FAZER A FINALIZACAO DE UMA RECEITA*/
                    request.setAttribute("id_receita", request.getParameter("id_receita"));
                    ArrayList<ItensReceita> listaItens = (ArrayList<ItensReceita>) request.getAttribute("listaItens");
                    String id_item[] = request.getParameterValues("id_item");
                    boolean ok = true;
                    for (String it : id_item) {
                        if (it == null) {
                            ok = false;
                            break;
                        }
                    }
                    if (ok) {
                        mens = dao.finalizarReceita(Integer.parseInt(request.getParameter("id_receita")));
                    } else {
                       mens = new Mensagens(false, "Você deve selecionar todos os itens da receita antes de finalizar");
                    }
                }else if (request.getParameter("pagina").contains(m.getREL_RECIBO_RECEITA())) {
                    //imprimir relatorio
                }
                request.setAttribute("btnFinalizarReceita", btnFinalizarReceita);

                //}
            }
        }
        return map.findForward("fwdRecFinalizaReceita");
    }
}
