package receita.servlet;

import aplicacao.modelo.Mensagens;
import aplicacao.modelo.Menu;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import profissional.dao.ValidaProfissional;
import receita.dao.ReceitaDAO;
import receita.dao.ValidaReceita;
import receita.modelo.ItensReceita;
import receita.modelo.ReceitasPrescritas;

/**
 *
 * @author Diogo
 */
public class MontarReceita extends Action {

    HttpSession session;

    @Override
    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ParseException {
        session = request.getSession(true);
        //if (session.getAttribute("login") == null) {
        //    return map.findForward("login");
        //} else {
        Menu m = new Menu();
        @SuppressWarnings("UnusedAssignment")
        Mensagens mens = null;
        String btnSalvarReceita = "#";//para tivar o botao de incluir itens e Salvar receita
        String buscaPaciente = "#";
        String recCadastradas = "#";
        request.setAttribute("menu", m);
        if (request.getParameter("pagina") != null) {
            if (ValidaProfissional.validaParametros(m, request.getParameter("pagina"))) {
                ReceitaDAO dao = new ReceitaDAO();

                /*BUSCAR PACIENTES CADASTRADOS*/
                if (request.getParameter("pagina").contains(m.getREC_BUSCAR_PACIENTE())) {
                    request.setAttribute("listaPacientes",
                            dao.buscarPacientes(request.getParameter("nome_paciente"),
                                    request.getParameter("num_prontuario"), request.getParameter("cpf")));
                } else if (request.getParameter("pagina").contains(m.getREC_BUSCAR_RECEITAS())) {
                    /*BUSCAR AS RECEITAS CADASTRADAS DE UM PACIENTE*/
                    request.setAttribute("listaPacientes",
                            request.getAttribute("listaPacientes"));
                    if (request.getParameter("id_paciente") != null) {
                        request.setAttribute("listaReceitas",
                                dao.buscarReceitas(Integer.parseInt(request.getParameter("id_paciente"))));
                    }
                } else if (request.getParameter("pagina").contains(m.getREC_NOVA())) {
                    /*CADASTRAR UMA RECEITA*/
                    request.setAttribute("id_paciente", request.getParameter("id_paciente"));
                    request.setAttribute("listaPacientes", request.getAttribute("listaPacientes"));
                    request.setAttribute("opcao", m.getREC_NOVA());
                    btnSalvarReceita = m.getREC_COMPLEMENTAR();

                } else if (request.getParameter("pagina").contains(m.getREC_COMPLEMENTAR())) {
                    /*COMPLEMENTAR UMA RECEITA CADASTRADA*/
                    request.setAttribute("listaPacientes", request.getAttribute("listaPacientes"));
                    if (request.getParameter("num_receita") != null) {
                        request.setAttribute("listaItens",
                                dao.buscarItensReceitas(Integer.parseInt(
                                        request.getParameter("num_receita"))));
                        request.setAttribute("opcao", "complementa");
                        btnSalvarReceita = m.getREC_COMPLEMENTAR();
                    }
                }

                if (btnSalvarReceita != "#") {
                    if (ValidaProfissional.validaParametrosOpcoes(m, request.getParameter("opcao"))) {
                        if (request.getParameter("opcao").contains(m.getREC_NOVA())) {
                            /*SALVAR NOVA RECEITA*/
                            mens = dao.addNovaReceita(
                                    (ArrayList<ItensReceita>) request.getAttribute("listaItens"),
                                    new ReceitasPrescritas(request));//adicionar metodo para receber itens
                            request.setAttribute("mensagem", mens);
                        } else if (request.getParameter("opcao").contains(m.getREC_COMPLEMENTAR())) {
                            /*SALVAR UMA RECEITA COMPLEMENTADA*/
                            mens = dao.atualizaReceita(
                                    (ArrayList<ItensReceita>) request.getAttribute("listaItens"),
                                    new ReceitasPrescritas(request));//adicionar metodo para receber itens
                            request.setAttribute("mensagem", mens);
                        } else if (request.getParameter("opcao").contains(m.getREC_ADD_ITEM())) {
                            /*ADICIONAR UM ITEM NA RECEITA*/
                            mens = ValidaReceita.validaCampos(request);
                            if (mens.ok) {
                                ArrayList<ItensReceita> itensReceita
                                        = (ArrayList<ItensReceita>) request.getAttribute("listaItens");
                                ItensReceita novoItem = new ItensReceita(request);
                                novoItem.setIdPrescritor(0);//adicionr o session do id do prescritor
                                mens = dao.verificaEstoque(novoItem);
                                if (mens.ok) {
                                    request.setAttribute("listaItens", dao.adicionaItensReceita(itensReceita, novoItem));
                                } else {
                                    request.setAttribute("listaItens", itensReceita);
                                }
                            }
                        }
                        request.setAttribute("mensagem", mens);
                    } else if (request.getParameter("opcao").contains(m.getREC_DEL_ITEM())) {
                        /*RETIRAR ITEM DA RECEITA*/
                        ArrayList<ItensReceita> itensReceita = (ArrayList<ItensReceita>) request.getAttribute("listaItens");
                        request.setAttribute("listaItens", dao.deletaItensReceita(itensReceita,
                                Integer.parseInt(request.getParameter("id_item"))));
                        request.setAttribute("mensagem", mens);
                    }
                }
            }
            request.setAttribute("buscaPaciente", buscaPaciente);
            request.setAttribute("btnSalvarReceita", btnSalvarReceita);
            request.setAttribute("recCadastradas", recCadastradas);

            //}
        }
        return map.findForward("fwdRecNovaReceita");
    }
}
